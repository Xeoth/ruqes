# RuqES

RuqES is a userscript, small piece of code which is installed in your browser and can alter how sites look and work, in this case [Ruqqus.com](https://ruqqus.com).

For a list of features and more details (e.g. installation), please see https://ruqqus.com/post/ldx/what-is-ruqes.

# Development

## Setup

Install appropriate [Node](https://nodejs.org) version (stated in [.node-version](.node-version)). You can [n](https://github.com/tj/n) or other Node managers.

```sh
$ npm i
```

## Watch mode

```sh
$ npm start
```

## Build

```sh
$ npm run build
```

Output is located in `dist` directory.

# TODO
- refactor processors to classes
- improve visuals of settings dialog - more space, organization (tabs, folds?)

# Ideas
- option to disable post action jump above top bar (`.post-actions:hover, .post-actions:focus { z-index: 4; }`)
- hide voted-on posts
- some repost check assistance (search by title, maybe by domain + title/url on ddg)
- post preview when creating new post (not sure how easy, I think Ruqqus is using bunch of custom stuff)
- customizable font size, font family
- card mode (auto-open expando button, don't load post content automatically, add new button to load post content)
- "my guilds" in sidebar could become an icon grid (user specifies guild names and the script replaces content with guild icons without names; customizable tags/icons?)
- change default sorting/limit of specific guild
- show only NSFW posts (infi scroll cd)
- counters with up/down-votes of other users
- stats of user's rep (chart)
- post/comment save (will be in Vue, not sure if it's worth it)

# License
AGPL3
