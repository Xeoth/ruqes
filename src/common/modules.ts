import {Config, readConfig} from 'common/config';
import {RuqESModule} from 'common/RuqESModule';
import {debugLog} from './common';

const modules: RuqESModule[] = [];

export interface ModuleSetupArgs {
  silent?: boolean;
}

export const registerClassModule = (cls: RuqESModule) => {
  cls.registerSomePostsFullyHiddenHandler(handleOnSomePostsFullyHidden)
  modules.push(cls);
}

const readConfigOrDefault = async (): Promise<Config> => await readConfig();

export const handleFirstSetupOfModules = async () => {
  debugLog('modules', 'handleFirstSetupOfModules');
  const cfg = await readConfigOrDefault();
  modules.forEach(m => m.setup({}, cfg));
};

export const handleModulesAfterContentChange = async () => {
  debugLog('modules', 'handleModulesAfterContentChange');
  const cfg = await readConfigOrDefault();
  modules.forEach(m =>
    m.onContentChange({silent: true}, cfg)
  );
};

const handleOnSomePostsFullyHidden = async () => {
  debugLog('modules', 'handleOnSomePostsFullyHidden');
  const cfg = await readConfigOrDefault();
  modules.forEach(m => m.onSomePostsFullyHidden(cfg))
};
