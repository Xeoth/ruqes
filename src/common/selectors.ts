import { pageLoadingCls } from 'common/styles';
import { $i } from 'common/common';

export const getPosts = () => $('#posts > .card, .posts > .card');
export const getPagination = (from?: JQuery) => $('ul.pagination', from).parent();
export const getNextPageLink = () => getPagination().find('a:contains(Next)');
export const getPageLoadingMarker = () => $(`.${pageLoadingCls}`);
export const getRightSidebarEl = () => $('.sidebar:not(.sidebar-left)');
export const getPostTitleInPostCreation = () => $i('post-title');
export const getPostUrlInPostCreation = () => $i('post-URL');
export const getTopLevelContainersForBlurring = () => $('#main-content-row, #submitform');
