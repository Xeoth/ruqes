import {$c, $i, debugLog, scriptVersion} from 'common/common';
import {
  blurAnimatedCls,
  blurCls,
  btnDangerCls,
  btnPrimaryCls,
  clsPrefix,
  creatorMarkCls,
  creatorNameCls,
  formCls,
  settingsBackdropCls,
  settingsCaptionCls,
  settingsExpandoButtonActionCharacterCls,
  settingsLogoCls,
  settingsModalCls,
  settingsModalVisibleCls,
  widthAutoCls,
} from 'common/styles';
import {
  asExpandoButtonStyleOrDefault,
  Config,
  defaultConfig,
  expandoButtonStyleNames,
  expandoButtonStyles,
  readConfig,
  writeConfig
} from 'common/config';
import logoSvg from 'assets/logo.svg';
import {download} from 'common/downloader';
import {getTopLevelContainersForBlurring} from 'common/selectors';
import ChangeEvent = JQuery.ChangeEvent;

const genSettingsId = (x: string) => `${clsPrefix}${x}`;

const expandoButtonId = genSettingsId('expando');
const expandoButtonStyleId = genSettingsId('expando-style');
const expandoButtonAlignRightId = genSettingsId('expando-align-right');
const expandoButtonResizeId = genSettingsId('expando-resize');
const expandoButtonTextClosedId = genSettingsId('expando-text-closed');
const expandoButtonTextClosedListId = genSettingsId('expando-text-closed-list');
const expandoButtonTextOpenedId = genSettingsId('expando-text-opened');
const expandoButtonTextOpenedListId = genSettingsId('expando-text-opened-list');
const expandoButtonShowCommentsId = genSettingsId('expando-show-comments');

const infiniteScrollId = genSettingsId('infinite-scroll');
const infiniteScrollLoadEarlierId = genSettingsId('infinite-scroll-load-earlier');
const bigVoteArrowsOnMobileId = genSettingsId('big-vote-arrows-on-mobile');
const voteEffectId = genSettingsId('vote-effect');
const openPostInNewTabId = genSettingsId('post-open-in-new-tab');
const hidePostsFromGuildsId = genSettingsId('post-hide-posts-from-guilds');
const fullyHidePostsFromGuildsId = genSettingsId('post-fully-hide-posts-from-guilds');
const hideAlreadyJoinedGuildsInDiscoveryId = genSettingsId('post-hide-already-joined-guilds');
const blurNsfwThumbnailsId = genSettingsId('post-blur-nsfw-thumbnails');
const sidebarRightHideButtonId = genSettingsId('sidebar-right-hide-button');
const sidebarRightAutoHideId = genSettingsId('sidebar-right-auto-hide');
const createPostLoadTitleId = genSettingsId('create-post-load-title');
const operationsContainerId = genSettingsId('operations');
const importHelperId = genSettingsId('import-helper');
const imgBbKeyId = genSettingsId('img-bb-key');
const commentCtrlEnterToSendId = genSettingsId('comment-ctrl-enter-to-send');
const improvedTableStylesId = genSettingsId('improved-table-styles');
const lessAggressivePatronBadgesId = genSettingsId('less-aggressive-patron-badges');

const debugId = genSettingsId('debug');
const debugAutoOpenSettingsId = genSettingsId('debug-auto-open-settings');
const debugSectionId = genSettingsId('debug-section');
const debugForceUpgradeId = genSettingsId('debug-force-upgrade');

const setVisibilityOfDebugSectionInSettings = (val: boolean) =>
  $i(debugSectionId).css({display: val ? 'block' : 'none'});

const initializeSettingsForm = (cfg: Config) => {
  const initChecked = (id: string, val: boolean) => $i(id).prop('checked', val);
  const initVal = (id: string, val: string) => $i(id).val(val);

  initChecked(expandoButtonId, cfg.expandoButton.enabled);
  initChecked(expandoButtonResizeId, cfg.expandoButton.resize);
  initVal(expandoButtonStyleId, cfg.expandoButton.style);
  initChecked(expandoButtonAlignRightId, cfg.expandoButton.alignRight);
  initVal(expandoButtonTextClosedId, cfg.expandoButton.textClosed);
  initVal(expandoButtonTextOpenedId, cfg.expandoButton.textOpened);
  initChecked(expandoButtonShowCommentsId, cfg.expandoButton.showComments);

  initChecked(infiniteScrollId, cfg.infiniteScroll.enabled);
  initChecked(infiniteScrollLoadEarlierId, cfg.infiniteScroll.loadEarlier);
  initChecked(bigVoteArrowsOnMobileId, cfg.voting.bigVoteArrowsOnMobile);
  initChecked(voteEffectId, cfg.voting.clickEffect);
  initChecked(openPostInNewTabId, cfg.post.openInNewTab);
  initVal(hidePostsFromGuildsId, cfg.post.hidePostsFromGuilds);
  initChecked(fullyHidePostsFromGuildsId, cfg.post.fullyHidePostsFromGuilds);
  initChecked(hideAlreadyJoinedGuildsInDiscoveryId, cfg.post.hideAlreadyJoinedGuildsInDiscovery);
  initChecked(blurNsfwThumbnailsId, cfg.post.blurNsfwThumbnails);
  initChecked(sidebarRightHideButtonId, cfg.sidebar.rightButton);
  initChecked(sidebarRightAutoHideId, cfg.sidebar.autoHideRight);
  initChecked(createPostLoadTitleId, cfg.createPost.loadTitleButton);
  initVal(imgBbKeyId, cfg.external.imgbbKey);
  initChecked(commentCtrlEnterToSendId, cfg.comment.ctrlEnterToSend);
  initChecked(improvedTableStylesId, cfg.post.improvedTableStyles);
  initChecked(lessAggressivePatronBadgesId, cfg.post.lessAggressivePatronBadges);

  const debugEnabled = cfg.debug.enabled;
  initChecked(debugId, debugEnabled);
  initChecked(debugAutoOpenSettingsId, cfg.debug.autoOpenSettings);
  setVisibilityOfDebugSectionInSettings(debugEnabled);
};

const openSettingsModal = async () => {
  getTopLevelContainersForBlurring().addClass(blurAnimatedCls).addClass(blurCls);
  initializeSettingsForm(await readConfig());
  $c(settingsBackdropCls).show();
  $c(settingsModalCls).addClass(settingsModalVisibleCls);
};

const closeSettingsModal = () => {
  getTopLevelContainersForBlurring().removeClass(blurCls);
  $c(settingsModalCls).removeClass(settingsModalVisibleCls);
  setTimeout(() => $c(settingsBackdropCls).hide(), 200); // wait for transition to finish
};

const getConfigFromSettingsForm = (): Omit<Config, 'scriptVersion'> => {
  const getStrVal = (id: string) => $i(id).val() as string;
  const getChecked = (id: string) => Boolean($i(id).prop('checked'));
  return ({
    expandoButton: {
      enabled: getChecked(expandoButtonId),
      resize: getChecked(expandoButtonResizeId),
      style: R.pipe(getStrVal, asExpandoButtonStyleOrDefault)(expandoButtonStyleId),
      alignRight: getChecked(expandoButtonAlignRightId),
      textClosed: getStrVal(expandoButtonTextClosedId),
      textOpened: getStrVal(expandoButtonTextOpenedId),
      showComments: getChecked(expandoButtonShowCommentsId),
    },
    infiniteScroll: {
      enabled: getChecked(infiniteScrollId),
      loadEarlier: getChecked(infiniteScrollLoadEarlierId),
    },
    voting: {
      bigVoteArrowsOnMobile: getChecked(bigVoteArrowsOnMobileId),
      clickEffect: getChecked(voteEffectId),
    },
    post: {
      openInNewTab: getChecked(openPostInNewTabId),
      hidePostsFromGuilds: getStrVal(hidePostsFromGuildsId),
      fullyHidePostsFromGuilds: getChecked(fullyHidePostsFromGuildsId),
      blurNsfwThumbnails: getChecked(blurNsfwThumbnailsId),
      hideAlreadyJoinedGuildsInDiscovery: getChecked(hideAlreadyJoinedGuildsInDiscoveryId),
      improvedTableStyles: getChecked(improvedTableStylesId),
      lessAggressivePatronBadges: getChecked(lessAggressivePatronBadgesId),
    },
    sidebar: {
      rightButton: getChecked(sidebarRightHideButtonId),
      autoHideRight: getChecked(sidebarRightAutoHideId),
    },
    createPost: {
      loadTitleButton: getChecked(createPostLoadTitleId),
    },
    comment: {
      ctrlEnterToSend: getChecked(commentCtrlEnterToSendId),
    },
    external: {
      imgbbKey: getStrVal(imgBbKeyId),
    },
    debug: {
      enabled: getChecked(debugId),
      autoOpenSettings: getChecked(debugAutoOpenSettingsId),
    },
  });
};

const saveAndCloseSettingsModal = async () => {
  const newValues = {...await readConfig(), ...getConfigFromSettingsForm()};
  debugLog('generated new config', newValues);
  await writeConfig(newValues);
  closeSettingsModal();
};

const saveAndReloadSettings = async () => {
  await saveAndCloseSettingsModal();
  window.location.reload();
};

const resetSettings = async () => {
  await writeConfig(defaultConfig);
  initializeSettingsForm(await readConfig());
}

const exportSettings = async () => {
  const cfg = await readConfig();
  download(JSON.stringify(cfg), 'application/json', 'RuqES-settings.json');
};

const importSettingsFileConfirmed = (evt: ChangeEvent) => {
  debugLog('importSettingsFileConfirmed', evt);
  const files = $i(importHelperId).prop('files');
  if (!files || !files[0]) {
    return;
  }
  const file = files[0];
  debugLog('got file', file);
  const reader = new FileReader();
  reader.onload = async (readEvt) => {
    const text = readEvt?.target?.result;
    debugLog('read result', text);
    let newCfg = null;
    try { newCfg = JSON.parse(text as any); } // 
    catch (e) { alert(`Failed to parse given file as JSON.`); }
    if (newCfg !== null) {
      await writeConfig(newCfg);
      window.location.reload();
    }
  }
  reader.readAsBinaryString(file);
};

const importSettings = async () => {
  $i(importHelperId).click();
};

const createSettingsDialog = () => {
  const withTooltip = (text: string, html: string) => (position = "bottom") =>
    `<span role="tooltip" aria-label="${text}" data-microtip-position="${position}">${html}</span>`;
  const mobileBadge = `<span class="badge badge-secondary">mobile</span>`;
  const desktopBadge = `<span class="badge badge-dark">desktop</span>`;
  const experimentalBadge = `<span class="badge badge-danger">experimental</span>`;
  const infiniteScrollCoolDownBadge = withTooltip('Adds cool-down to infinite scroll', `<span class="badge badge-warning">IS CD</span>`);
  const expandoButtonStyleOptions = expandoButtonStyles.map(x => `<option value="${x}">${expandoButtonStyleNames[x]}</option>`).join('\n');
  const content = `
<hr class="mt-0 mb-2">
<form class="${formCls}">
<div class="form-group mb-2">
    <input type="checkbox" id="${expandoButtonId}" class="form-control" />
    <label for="${expandoButtonId}">Expando button</label>
</div>
<div class="form-group mb-2 ml-3">
    <label for="${expandoButtonStyleId}" class="mr-1">Style</label>
    <select id="${expandoButtonStyleId}" class="form-control">
        ${expandoButtonStyleOptions}
    </select>
</div>
<div class="form-group mb-2 ml-3">
    <input type="checkbox" id="${expandoButtonShowCommentsId}" class="form-control" />
    <label for="${expandoButtonShowCommentsId}" class="mr-1">Show comments</label>${experimentalBadge}
</div>
<div class="form-group mb-2 ml-3">
    <input type="checkbox" id="${expandoButtonResizeId}" class="form-control" />
    <label for="${expandoButtonResizeId}">Image resizing ${desktopBadge}</label>
</div>
<div class="form-group mb-2 ml-3">
    <input type="checkbox" id="${expandoButtonAlignRightId}" class="form-control" />
    <label for="${expandoButtonAlignRightId}">Align right ${mobileBadge}</label>
</div>
<div class="form-group mb-2 ml-3 ${settingsExpandoButtonActionCharacterCls}">
    <label for="${expandoButtonTextClosedId}" class="mr-1">Open character:</label><br />
    <input type="text" id="${expandoButtonTextClosedId}" class="form-control ${widthAutoCls}" maxlength="5" list="${expandoButtonTextClosedListId}" />
    <datalist id="${expandoButtonTextClosedListId}">
        <option value="+">
        <option value="☩">
        <option value="➕">
        <option value="ᚐ">
        <option value="⧾">
        <option value="⊕">
        <option value="≡">
        <option value="≡">
        <option value="𝍢">
        <option value="≣">
        <option value="𝍣">
        <option value="𝌉">
    </datalist>
</div>
<div class="form-group mb-2 ml-3 ${settingsExpandoButtonActionCharacterCls}">
    <label for="${expandoButtonTextOpenedId}" class="mr-1">Close character:</label><br />
    <input type="text" id="${expandoButtonTextOpenedId}" class="form-control ${widthAutoCls}" maxlength="5" list="${expandoButtonTextOpenedListId}" />
        <datalist id="${expandoButtonTextOpenedListId}">
        <option value="-">
        <option value="–">
        <option value="―">
        <option value="⧿">
        <option value="⧿">
        <option value="➖">
        <option value="⊝">
        <option value="Θ">
    </datalist>
</div>

<div class="form-group mb-2">
    <input type="checkbox" id="${infiniteScrollId}" class="form-control" />
    <label for="${infiniteScrollId}">Infinite scroll</label>
</div>
<div class="form-group mb-2 ml-3">
    <input type="checkbox" id="${infiniteScrollLoadEarlierId}" class="form-control" />
    <label for="${infiniteScrollLoadEarlierId}" class="mr-1">Start loading next page earlier</label>${infiniteScrollCoolDownBadge('left')}
</div>

<div class="form-group mb-2">
    <input type="checkbox" id="${bigVoteArrowsOnMobileId}" class="form-control" />
    <label for="${bigVoteArrowsOnMobileId}">Bigger vote arrows ${mobileBadge}</label>
</div>

<div class="form-group mb-2">
    <input type="checkbox" id="${voteEffectId}" class="form-control" />
    <label for="${voteEffectId}">Vote effect ${mobileBadge}</label>
</div>

<div class="form-group mb-2">
    <input type="checkbox" id="${improvedTableStylesId}" class="form-control" />
    <label for="${improvedTableStylesId}">Improved styles for tables in posts</label>
</div>

<div class="form-group mb-2">
    <input type="checkbox" id="${openPostInNewTabId}" class="form-control" />
    <label for="${openPostInNewTabId}">Open posts in a new tab</label>
</div>

<div class="form-group mb-2 d-block">
    <label for="${hidePostsFromGuildsId}">Semi-hide posts from guilds:</label><br />
    <input type="text" id="${hidePostsFromGuildsId}" class="form-control" placeholder="Guild names without '+' separated by comma ','" />
</div>
<div class="form-group mb-2 ml-3">
    <input type="checkbox" id="${fullyHidePostsFromGuildsId}" class="form-control" />
    <label for="${fullyHidePostsFromGuildsId}" class="mr-1">Fully hide</label> ${infiniteScrollCoolDownBadge()}
</div>

<div class="form-group mb-2">
    <input type="checkbox" id="${hideAlreadyJoinedGuildsInDiscoveryId}" class="form-control" />
    <label for="${hideAlreadyJoinedGuildsInDiscoveryId}" class="mr-1">Hide joined guilds on "discover" page</label> ${infiniteScrollCoolDownBadge('left')}
</div>

<div class="form-group mb-2">
    <input type="checkbox" id="${blurNsfwThumbnailsId}" class="form-control" />
    <label for="${blurNsfwThumbnailsId}">Blur thumbnails of NSFW posts</label>
</div>

<div class="form-group mb-2">
    <input type="checkbox" id="${sidebarRightHideButtonId}" class="form-control" />
    <label for="${sidebarRightHideButtonId}">Enable hide button for right sidebar</label>
</div>
<div class="form-group mb-2 ml-3">
    <input type="checkbox" id="${sidebarRightAutoHideId}" class="form-control" />
    <label for="${sidebarRightAutoHideId}">Auto-hide right sidebar</label>
</div>

<div class="form-group mb-2">
    <input type="checkbox" id="${createPostLoadTitleId}" class="form-control" />
    <label for="${createPostLoadTitleId}">"Load title" button on create post page</label>
</div>

<div class="form-group mb-2 d-block">
    <label for="${imgBbKeyId}">
        <a href="https://api.imgbb.com/" target="_blank">imgbb</a> API key:
    </label><br />
    <input type="text" id="${imgBbKeyId}" class="form-control" />
</div>

<div class="form-group mb-2">
    <input type="checkbox" id="${commentCtrlEnterToSendId}" class="form-control" />
    <label for="${commentCtrlEnterToSendId}"><kbd>CTRL</kbd> + <kbd>ENTER</kbd> to send comment</label>
</div>

<div class="form-group mb-2">
    <input type="checkbox" id="${lessAggressivePatronBadgesId}" class="form-control" />
    <label for="${lessAggressivePatronBadgesId}">Less aggressive patron badges</label>
</div>

<hr class="my-1">

<div class="form-group mb-1" style="opacity: 0.5">
    <input type="checkbox" id="${debugId}" class="form-control" />
    <label for="${debugId}">Debug mode</label>
</div>
<div id="${debugSectionId}" class="ml-3">
    <div class="form-group mb-2">
        <input type="checkbox" id="${debugAutoOpenSettingsId}" class="form-control" />
        <label for="${debugAutoOpenSettingsId}">Auto-open settings</label>
    </div>
    <a class="btn btn-primary ${btnPrimaryCls} mb-2" id="${debugForceUpgradeId}">Force config upgrade</a>
</div>

<hr class="my-1">

<div class="mb-1"><strong>Settings operations</strong></div>
<div id="${operationsContainerId}" class="d-flex">
    <input id="${importHelperId}" type="file" class="d-none" accept="application/json">
</div>

<hr class="my-2">

<div>Changes will take effect after a page reload.</div>

<hr class="my-2">

</form>
    `;
  const captionPart = `
<div class="d-flex justify-content-center align-items-baseline">
<div class="${settingsLogoCls}">${logoSvg}</div>
<h1 class="${settingsCaptionCls}">RuqES</h1>
<div class="ml-1" style="opacity: 0.5">
  <span class="position-absolute" style="top: 0.8em">${scriptVersion}</span>
  by <a href="https://ruqqus.com/@enefi">@enefi</a>
</div>
</div>
    `
  const contentEl = $(`<div>${content}</div>`);
  contentEl.find('form').submit(() => {
    saveAndReloadSettings();
    return false;
  });
  const cancelButton = $('<a class="btn btn-secondary">Cancel</a>').click(closeSettingsModal);
  const saveButton = $('<a class="btn btn-secondary">Save</a>').click(saveAndCloseSettingsModal);
  const saveAndReloadButton =
    $('<a class="btn btn-primary ml-2">Save & reload</a>').addClass(btnPrimaryCls).click(saveAndReloadSettings);
  const exportButton = $('<a class="btn btn-secondary">Export</a>').click(exportSettings);
  const importButton = $('<a class="btn btn-secondary ml-2">Import</a>').click(importSettings);
  const resetButton = $('<a class="btn btn-danger ml-2">Reset</a>').addClass(btnDangerCls).click(resetSettings);
  contentEl.find(`#${operationsContainerId}`)
    .append(exportButton)
    .append(importButton)
    .append(resetButton)
  ;
  contentEl.find(`#${importHelperId}`).change(importSettingsFileConfirmed);
  const modal = $('<div>')
    .addClass(settingsModalCls)
    .append(captionPart)
    .append(contentEl)
    .append($('<div>').addClass(['d-flex', 'justify-content-between', 'mt-2'])
      .append(cancelButton)
      .append($('<div>')
        .append(saveButton)
        .append(saveAndReloadButton),
      ),
    )
  ;
  $i(debugForceUpgradeId, modal).click(() => readConfig({forceUpgrade: true}));
  $i(debugId, modal).change(x => setVisibilityOfDebugSectionInSettings((x.target as HTMLInputElement).checked));
  const backdrop = $('<div>')
    .addClass(settingsBackdropCls)
    .click(evt => {
      if ($(evt.target).hasClass(settingsBackdropCls)) {
        closeSettingsModal();
      }
    });
  return backdrop.append(modal).hide();
};

const setupCreatorMark = () => {
  const logoEl = $('<span>').html(logoSvg).addClass(creatorMarkCls).prop('title', 'RuqES developer');
  $c('user-name').filter((_, el) => $(el).text() === 'enefi').addClass(creatorNameCls).after(logoEl);
};

const createSettingsButtonForDesktop = () => {
  const icon = $('<i class="fas fa-wrench fa-width-rem text-left mr-3"></i>');
  return $('<a>')
    .prop('class', 'dropdown-item')
    .text('RuqES')
    .prepend(icon)
    .prop('href', 'javascript:void(0)')
    .click(openSettingsModal)
    ;
};

const createSettingsButtonForMobile = () => {
  const icon = $('<i class="fas fa-wrench fa-width-rem mr-3">');
  const link = $('<a>')
    .addClass('nav-link')
    .append(icon)
    .append('RuqES')
    .click(openSettingsModal)
    .click(() => $('.navbar-toggler').click())
  ;
  return $('<li class="nav-item">').append(link);
}

export const setupSettings = async () => {
  const cfg = await readConfig();
  debugLog('setupSettings', cfg);
  setupCreatorMark();
  $('#navbar #navbarResponsive > ul > li:last-child a[href="/settings"]').after(createSettingsButtonForDesktop());
  $('#navbarResponsive > ul:last-child a.nav-link[href="/settings"]').parent().after(createSettingsButtonForMobile());
  $('body').prepend(createSettingsDialog());
  if (cfg.debug.autoOpenSettings) { await openSettingsModal(); }
};
