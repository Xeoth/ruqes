import {loadingDotsCls, ruqesMarkCls, textLoaderCls} from 'common/styles';
import logoSvg from 'assets/logo.svg';

export const logPrefix = '[RuqES]';
export const scriptInfo = GM.info.script
export const scriptVersion = scriptInfo.version;

let debugEnabled = false;
export const isDebugEnabled = () => debugEnabled;
export const enabledDebug = () => debugEnabled = true;

export const debugLog = (...xs: any[]) => isDebugEnabled() && console.log(logPrefix, ...xs);
export const printLog = (...xs: any[]) => console.log(logPrefix, ...xs);
export const printError = (...xs: any[]) => console.error(logPrefix, ...xs);
export const $c = (cls: string) => $(`.${cls}`);
export const $i = (cls: string, parent?: JQuery) => $(`#${cls}`, parent);
export const setClass = (el: JQuery, cls: string, value: boolean) => {
  if (value) {el.addClass(cls);} else {el.removeClass(cls);}
  return el;
};

export const createLoadingDots = () => {
  const container = $('<span>').addClass(loadingDotsCls);
  R.range(0, 3).map(() => $('<i>.</i>')).forEach(x => container.append(x));
  return container;
};

export const createRuqesMark = () => $('<span>').html(logoSvg).addClass(ruqesMarkCls);

export const createTextLoader = (text: string, cls: string) =>
  $('<div>')
    .addClass(textLoaderCls)
    .addClass(cls)
    .append(createRuqesMark())
    .append($('<span>').text(text).css('margin-left', '0.33em'))
    .append(createLoadingDots())

export const isDarkTheme = () => $('#dark-switch').hasClass('fa-toggle-on');

export const xhr = (cfg: GM.Request) => GM.xmlHttpRequest(cfg);

export const preventDefaults = (evt: Event | JQuery.Event) => {
  evt.preventDefault();
  evt.stopPropagation();
};

export const dropWhileString = (pred: (x: string) => boolean, y: string): string => (R.dropWhile as any)(pred, y);
