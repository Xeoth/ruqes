import microtip from 'microtip/microtip.css';

const addStyle = (style: string | string[]) => {
  style = style instanceof Array ? style.join('\n') : style;
  $('head').append($('<style type="text/css">' + style + '</style>'));
}
export const clsPrefix = 'RuqES-by-enefi--';
export const genClsName = (x: string) => clsPrefix + x;
export const expandBoxCls = genClsName('box');
export const expandBoxOpenedCls = genClsName('box-opened');
export const boxEmptyCls = genClsName('box-empty');
export const expandBtnCls = genClsName('expando-button');
export const boxImgCls = genClsName('box-img');
export const boxEmbedCls = genClsName('box-embed');
export const boxPostTextCls = genClsName('box-post-text');
export const boxPostCommentsCls = genClsName('box-post-comments');
export const pageLoadingCls = genClsName('page-loader');
export const postLoadingCls = genClsName('post-loader');
export const loadingDotsCls = genClsName('loading-dots');
export const codeBlockCls = genClsName('code-block');
export const textLoaderCls = genClsName('text-loader');
export const errorTextCls = genClsName('error-text');
export const pageLoadingErrorCls = genClsName('page-loading-error');
export const ruqesMarkCls = genClsName('ruqes-mark');
export const creatorMarkCls = genClsName('creator-mark');
export const creatorNameCls = genClsName('creator-name');
export const settingsBackdropCls = genClsName('settings-backdrop');
export const settingsModalCls = genClsName('settings-modal');
export const settingsModalVisibleCls = genClsName('settings-modal-shown');
export const settingsCaptionCls = genClsName('settings-caption');
export const settingsLogoCls = genClsName('settings-logo');
export const settingsExpandoButtonActionCharacterCls = genClsName('settings-expando-action-char');
export const formCls = genClsName('form');
export const blurAnimatedCls = genClsName('blur-anim');
export const blurCls = genClsName('blur');
export const voteAnimCls = genClsName('vote-anim');
export const semiHiddenPostFromGuildCls = genClsName('semi-hidden-post-from-guild');
export const sidebarButtonIconCls = genClsName('sidebar-button-icon');
export const sidebarClosedCls = genClsName('sidebar-closed');
export const btnPrimaryCls = genClsName('btn-primary');
export const btnDangerCls = genClsName('btn-danger');
export const loadTitleButtonCls = genClsName('post-load-title-button');
export const loadTitleButtonLoadingCls = genClsName('post-load-title-button-loading');
export const uploadImageToImgbbButtonCls = genClsName('post-upload-imgbb-button');
export const uploadImageToImgbbButtonHighlightedCls = genClsName('post-upload-imgbb-button-highlighted');
export const uploadImageToImgbbButtonLoadingCls = genClsName('post-upload-imgbb-button-loading');
export const blurNsfwThumbnailsCls = genClsName('blur-nsfw-thumbnails');
export const widthAutoCls = genClsName('width-auto');
export const infiniteScrollCoolDownMarkerCls = genClsName('inifinite-scroll-cooldown-marker');
export const improvedTableCls = genClsName('improved-table');
export const nonAggressiveCls = genClsName('non-aggressive');

export const setupStyles = (isDarkTheme: boolean) => {
  const primaryColor = 'rgba(128, 90, 213, 1)';
  const ruqesColor = '#800080';
  const containerBorderColor = isDarkTheme ? '#303030' : '#E2E8F0';
  const containerBackgroundColor = isDarkTheme ? '#181818' : '#D2D8E0';
  const containerBackgroundColorSecondary = isDarkTheme ? '#232323' : '#c8ced6';
  const textColor = isDarkTheme ? 'white' : 'black';
  const arrowVoteColor = isDarkTheme ? '#303030' : '#cbd5e0';
  const upvoteColor = '#805ad5';
  const downvoteColor = '#38b2ac';
  addStyle(microtip);
  addStyle(`
.${widthAutoCls} { width: auto; }
  
.${expandBtnCls} {
  margin: -0.4em 1em -0.4em 0;
  padding: 0;
  display: flex;
  align-items: center;
  justify-content: center;
}

.${expandBtnCls}:last-child {
  margin: -0.4em 0 -0.4em 1em;
}

.${expandBtnCls}--style-bigWide {
  width: 3.25em;
  height: 2.5em;
}
.${expandBtnCls}--style-big {
  width: 2.5em;
  height: 2.5em;
}
.${expandBtnCls}--style-medium {
  width: 2em;
  height: 2em;
  font-size: 90%;
}
.${expandBtnCls}--style-small {
  width: 1.5em;
  height: auto;
  margin: 0 0.5em 0 0;
  border: 0;
  background: none;
}

.${expandBoxCls} {
  display: none;
  max-width: 100%;
  border: 1px solid ${containerBorderColor};
  margin-top: 1em;
  padding-left: 0;
  padding-right: 0;
}

.${expandBoxOpenedCls} {
  display: flex;
  flex-direction: column;
}

img.${boxImgCls} {
  max-width: 100%;
}

.stretched-link::after { display: none; }

.${boxEmbedCls} {
  border-left: 1px solid ${containerBorderColor};
  order: 1;
}

.${boxPostTextCls} {
  border-left: 1px solid ${containerBorderColor};
  order: 0;
  padding: 5px;
}

.${boxPostTextCls} p:last-child {
  margin-bottom: 0;
}

.${boxPostCommentsCls} {
}

.${boxPostCommentsCls} .comment-actions > ul > li.arrow-up::before,
.${boxPostCommentsCls} .comment-actions > ul > li.arrow-down::before {
  color: ${arrowVoteColor};
}

.${boxPostCommentsCls} .comment-actions.upvoted > ul > li.arrow-up::before {
  color: ${upvoteColor};
}

.${boxPostCommentsCls} .comment-actions.downvoted > ul > li.arrow-down::before {
  color: ${downvoteColor};
}

@keyframes ruqesLoadingDots {
  0% {
    transform: translate(0, 0) scale(1);
    color: ${primaryColor}
  }
  25% {
    transform: translate(0, -0.2em) scale(1.33);
  }
  50% {
    transform: translate(0, 0) scale(1);
  }
  55% {
  }
  65% {
    color: ${textColor}
  }
}

.${loadingDotsCls} i {
  animation-name: ruqesLoadingDots;
  animation-duration: 2s;
  animation-delay: 0s;
  animation-iteration-count: infinite;
  animation-timing-function: ease-in-out;
  display: inline-block;
}

.${loadingDotsCls} i:nth-child(2) { animation-delay: 0.2s; }
.${loadingDotsCls} i:nth-child(3) { animation-delay: 0.4s; }

.${codeBlockCls} {
  border: 1px solid ${containerBorderColor};
  padding: 1em;
  font-size: 66%;
  background-color: lightgray;
}

.${textLoaderCls} {
  font-weight: bold;
}

.${pageLoadingCls} {
  margin: 1em 0 3em 0;
  text-align: center;
}

.${errorTextCls} {
  font-weight: bold;
  color: red;
}

.${pageLoadingErrorCls} {
  text-align: left;
}

.${ruqesMarkCls} svg {
  width: 1.2em;
  height: 1.2em;
}

.${creatorMarkCls} svg {
  width: 1.2em;
  height: 1.2em;
}

.${creatorNameCls} {
  transition: color 0.2s;
  color: ${isDarkTheme ? '#bf40bf' : '#800080'};
}
.${creatorNameCls}:hover {
  color: ${textColor};
}

.${settingsBackdropCls} {
  position: fixed;
  width: 100%;
  height: 100%;
  background-color: ${`${containerBackgroundColor}66`};
  z-index: 10000;
  top: 0;
}

.${settingsModalCls}::-webkit-scrollbar {
    height: 5px;
    width: 5px;
    background: ${containerBorderColor};
}
.${settingsModalCls}::-webkit-scrollbar-thumb {
    background: ${containerBackgroundColor};
}
.${settingsModalCls} {
  position: fixed;
  left: 50%;
  top: 50%;
  background-color: ${containerBorderColor};
  border: 2px solid ${containerBackgroundColor};
  border-radius: 5px;
  padding: 0.5em;
  transform: translate(-50%, -50%) scale(0.8);
  min-width: 420px;
  overflow-y: auto;
  max-height: 100%;
  opacity: 0;
  transition: transform 0.2s, opacity 0.1s;
  scrollbar-color: ${containerBackgroundColor} ${containerBorderColor};
  scrollbar-width: thin;
}
.${settingsModalVisibleCls} {
  transform: translate(-50%, -50%) scale(1);
  opacity: 1;
}
@media (max-width: 575.98px) {
  .${settingsModalCls} {
    width: 100%;
    min-width: auto;
  }
}

.${formCls} input[type="checkbox"] {
  width: auto;
  height: auto;
  display: inline-block;
  margin-right: 0.3em;
}

.${formCls} label {
  margin-bottom: 0;
}

.${formCls} .form-group {
  margin-bottom: 0;
  display: flex;
  align-items: center;
}

.${blurAnimatedCls} {
  filter: blur(0);
  transition: filter 0.5s ease-in-out;
}

.${blurCls} {
  filter: blur(2px);
}

.${settingsCaptionCls} {
  text-shadow: ${isDarkTheme ? 'black' : '#999'} 1px 0.5px 0px;
}

.${settingsLogoCls} {
  display: flex;
}
.${settingsLogoCls} svg {
  width: 2em;
  height: 2em;
  margin-right: 0.2em;
}

.${settingsExpandoButtonActionCharacterCls} input {
  width: 4em;
}
.${settingsExpandoButtonActionCharacterCls} label {
  width: 10em;
}

@keyframes ruqesVoteEffect {
  0% {
    transform: scale(1);
    opacity: 0;
  }
  1% {
    opacity: 1;
  }
  90% {
    transform: scale(3);
  }
  100% {
    transform: scale(3.1);
    opacity: 0;
  }
}

.${voteAnimCls} {
  animation-name: ruqesVoteEffect;
  animation-duration: 1s;
  animation-delay: 0s;
  animation-iteration-count: 1;
  animation-timing-function: ease-in-out;
  position: absolute !important;
  left: 6%;
  top: 20%;
}

.${semiHiddenPostFromGuildCls} {
  opacity: 0.2;
  transition: opacity 0.33s;
  max-height: 1.5em;
  background-color: ${containerBackgroundColor};
  overflow: hidden;
  padding-top: 0 !important;
}
.${semiHiddenPostFromGuildCls}:hover {
  opacity: 1;
  max-height: none;
}

.${sidebarButtonIconCls}::before {
    cursor: pointer;
    font-size: 1rem;
    color: #777;
    font-family: "Font Awesome 5 Pro";
    font-weight: 900;
    content: "";
}

.${sidebarButtonIconCls}.opened::before {
    cursor: pointer;
    font-size: 1rem;
    color: #777;
    font-family: "Font Awesome 5 Pro";
    font-weight: 900;
    content: "";
}

.${sidebarClosedCls} {
    max-width: 3em;
    padding: 0;
}
.${sidebarClosedCls} > :not(:first-child) {
    display: none !important;
}

.${btnPrimaryCls} {
    color: rgb(207, 207, 207) !important;
    background-color: #6133c9;
    border-color: #5c31bf;
}

.${btnPrimaryCls}:not(:disabled):not(.disabled):active, .${btnPrimaryCls}:not(:disabled):not(.disabled).active, .show>.${btnPrimaryCls}.dropdown-toggle {
    color: #fff !important;
    background-color: #6133c9;
    border-color: #5c31bf;
}

.${btnDangerCls} {
    color: rgb(207, 207, 207) !important;
    background-color: #e53e3e;
    border-color: #e53e3e;
}

@keyframes ruqesSpinning {
  0% {
    transform: scale(1) rotate(0deg);
  }
  30% {
    transform: scale(1.2) rotate(108deg);
  }
  60% {
    transform: scale(0.8) rotate(216deg);
  }
  100% {
    transform: scale(1) rotate(360deg);
  }
}

.${loadTitleButtonCls} {
  margin-top: -0.4em;
}
.${loadTitleButtonCls} svg {
  width: 1.2em;
  height: 1.2em;
  margin-left: -0.3em;
  margin-right: 0.2em;
}
.${loadTitleButtonCls}.${loadTitleButtonLoadingCls} svg {
  animation: 1s infinite linear ruqesSpinning;
}

.${uploadImageToImgbbButtonCls} {
  margin-top: -0.4em;
  border: 1px dashed ${ruqesColor};
}
.${uploadImageToImgbbButtonCls} svg {
  width: 1.2em;
  height: 1.2em;
  margin-left: -0.3em;
  margin-right: 0.2em;
}
.${uploadImageToImgbbButtonCls}.${uploadImageToImgbbButtonLoadingCls} svg {
  animation: 1s infinite linear ruqesSpinning;
}

.${loadTitleButtonCls} ~ .${uploadImageToImgbbButtonCls} {
  margin-left: 0.5em;
}

.${uploadImageToImgbbButtonHighlightedCls} {
  border-style: solid;
}

body.${blurNsfwThumbnailsCls} .card.nsfw .post-img {
  filter: blur(7px) saturate(0.3);
}

.${infiniteScrollCoolDownMarkerCls} {
  font-size: 130%;
  margin-left: 1em;
}

.${improvedTableCls} td, .${improvedTableCls} th {
  border: 1px solid ${containerBorderColor};
  padding: 0.2em 0.5em;
}
.${improvedTableCls} th {
  background-color: ${containerBackgroundColorSecondary};
}

.${nonAggressiveCls} {
  opacity: 0.33;
  transition: opacity 0.2s;
}
.${nonAggressiveCls}:hover {
  opacity: 1;
}

`);
};
