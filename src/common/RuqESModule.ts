import {Config} from './config';
import {ModuleSetupArgs} from './modules';

export type SomePostsFullyHiddenHandler = () => void;

export abstract class RuqESModule {
  protected somePostsFullyHiddenCb: SomePostsFullyHiddenHandler | null = null;

  abstract async setup(args: ModuleSetupArgs, cfg: Config): Promise<unknown>;

  async onContentChange(args: ModuleSetupArgs, cfg: Config): Promise<unknown> {
    return Promise.resolve();
  }

  registerSomePostsFullyHiddenHandler(handler: SomePostsFullyHiddenHandler) {
    if (this.somePostsFullyHiddenCb) {
      throw new Error(`somePostsFullyHidden handler already registered`);
    }
    this.somePostsFullyHiddenCb = handler;
  }

  onSomePostsFullyHidden(cfg: Config): void {}
}
