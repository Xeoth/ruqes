import {debugLog, enabledDebug, scriptVersion} from 'common/common';
import {DeepPartial} from '../types/DeepPartial';

const settingsConfigKey = 'config';

export const expandoButtonStyles = ['bigWide', 'big', 'medium', 'small'] as const;
export type ExpandoButtonStyle = typeof expandoButtonStyles[number];
export const expandoButtonStyleNames: Record<ExpandoButtonStyle, string> = {
  small: 'Small (icon)',
  medium: 'Medium',
  big: 'Big',
  bigWide: 'Big - extra wide'
}
export const defaultExpandoButtonStyle = 'big';
export const asExpandoButtonStyleOrDefault = (x: string): ExpandoButtonStyle =>
  R.includes(x, expandoButtonStyles) ? x as ExpandoButtonStyle : defaultExpandoButtonStyle;

export type Config = {
  scriptVersion: string,
  debug: {
    enabled: boolean,
    autoOpenSettings: boolean,
  },
  expandoButton: {
    enabled: boolean,
    resize: boolean,
    style: ExpandoButtonStyle,
    alignRight: boolean,
    textClosed: string,
    textOpened: string,
    showComments: boolean,
  },
  infiniteScroll: {
    enabled: boolean,
    loadEarlier: boolean,
  },
  voting: {
    bigVoteArrowsOnMobile: boolean,
    clickEffect: boolean,
  },
  post: {
    openInNewTab: boolean,
    hidePostsFromGuilds: string,
    fullyHidePostsFromGuilds: boolean,
    blurNsfwThumbnails: boolean,
    hideAlreadyJoinedGuildsInDiscovery: boolean,
    improvedTableStyles: boolean,
    lessAggressivePatronBadges: boolean,
  },
  createPost: {
    loadTitleButton: boolean,
  },
  comment: {
    ctrlEnterToSend: boolean,
  },
  sidebar: {
    rightButton: boolean,
    autoHideRight: boolean,
  },
  external: {
    imgbbKey: string,
  }
};

export type PartialConfig = DeepPartial<Config>;

export const defaultConfig: Config = Object.freeze({
  scriptVersion,
  debug: {
    enabled: false,
    autoOpenSettings: false,
  },
  expandoButton: {
    enabled: true,
    resize: true,
    style: defaultExpandoButtonStyle,
    alignRight: false,
    textClosed: '+',
    textOpened: '-',
    showComments: true,
  },
  infiniteScroll: {
    enabled: true,
    loadEarlier: false,
  },
  voting: {
    bigVoteArrowsOnMobile: true,
    clickEffect: true,
  },
  post: {
    openInNewTab: false,
    hidePostsFromGuilds: '',
    fullyHidePostsFromGuilds: false,
    blurNsfwThumbnails: false,
    hideAlreadyJoinedGuildsInDiscovery: true,
    improvedTableStyles: true,
    lessAggressivePatronBadges: false,
  },
  createPost: {
    loadTitleButton: true,
  },
  comment: {
    ctrlEnterToSend: true,
  },
  sidebar: {
    rightButton: true,
    autoHideRight: false,
  },
  external: {
    imgbbKey: '',
  }
});

const upgradeConfig = (cfg: PartialConfig): Config => {
  return R.pipe(
    R.mergeDeepLeft(cfg),
    R.assoc('scriptVersion', scriptVersion),
  )(defaultConfig);
};

interface ReadConfigArgs {
  forceUpgrade?: boolean;
}

export const readConfig = async ({forceUpgrade}: ReadConfigArgs = {}): Promise<Config> => {
  const rawCfg = await GM.getValue(settingsConfigKey, defaultConfig) as PartialConfig;
  let cfg: Config;
  if (rawCfg.scriptVersion !== scriptVersion || forceUpgrade) {
    console.log(`[RuqES] Upgrading config ${rawCfg.scriptVersion} -> ${scriptVersion}`);
    const upgradedConfig = upgradeConfig(rawCfg);
    cfg = upgradedConfig;
    await writeConfig(upgradedConfig);
  } else {
    cfg = rawCfg as Config;
  }
  debugLog('readConfig', cfg);
  if (cfg.debug) {
    enabledDebug();
  }
  return cfg;
};

export const writeConfig = async (x: Config) =>
  await GM.setValue(settingsConfigKey, x as any); // value can be an object, types are incorrect
