import {debugLog, printError} from 'common/common';
import {clsPrefix} from 'common/styles';
import {RuqESModule} from '../common/RuqESModule';
import {ModuleSetupArgs} from '../common/modules';
import {Config} from '../common/config';

const genCommentId = (x: string) => `${clsPrefix}${x}`;

const processedCommentBoxMarkCls = genCommentId('processed-comment-box');

export class CommentModule extends RuqESModule {
  setupCtrlEnterToSend() {
    $('.comment-box')
      .filter((_, el) => !$(el).hasClass(processedCommentBoxMarkCls))
      .keydown(evt => {
        if (evt.key === 'Enter' && (evt.ctrlKey || evt.metaKey)) {
          debugLog('setupCtrlEnterToSend', 'CTRL+Enter detected', evt);
          const btn = $(evt.target).parent().find('a:contains(Comment)');
          if (btn.length > 1) {
            printError('setupCtrlEnterToSend', 'found multiple send buttons', btn);
          } else if (btn.length === 0) {
            printError('setupCtrlEnterToSend', 'no send button found', btn);
          } else {
            debugLog('setupCtrlEnterToSend', 'clicking on send button', btn);
            btn.click();
          }
        }
      })
      .addClass(processedCommentBoxMarkCls);
  };

  async setup(args: ModuleSetupArgs, cfg: Config) {
    const commentCfg = cfg.comment || {};
    if (!R.prop('silent', args)) {
      debugLog('setupComment', commentCfg);
    }
    if (commentCfg.ctrlEnterToSend) {
      setInterval(this.setupCtrlEnterToSend, 1000);
    }
  }
}
