import {
  blurNsfwThumbnailsCls,
  boxPostTextCls,
  semiHiddenPostFromGuildCls,
  improvedTableCls,
  nonAggressiveCls
} from 'common/styles';
import {debugLog, dropWhileString} from 'common/common';
import {Config} from 'common/config';
import {ModuleSetupArgs} from 'common/modules';
import {RuqESModule} from 'common/RuqESModule';

export class PostsModule extends RuqESModule {
  private isGuildHidden = (cfg: Config['post'], guildName = '') => {
    const hiddenGuilds = (cfg?.hidePostsFromGuilds || '').split(',').map(x => x.toLowerCase());
    const processedGuildName = dropWhileString(x => x === '+', guildName.toLowerCase());
    return hiddenGuilds.includes(processedGuildName);
  }

  private isPostFromHiddenGuild = (cfg: Config['post']) => (_: unknown, el: HTMLElement) => {
    const guildName =
      $(el).find('.post-meta-guild a').filter((_, el) => $(el).text().startsWith('+')).first().text();
    return this.isGuildHidden(cfg, guildName);
  };

  private isOnDiscoverPage = () => window.location.pathname === '/browse';

  private hideAlreadyJoinedGuildTiles() {
    $('#main-content-col .card-footer :not(.d-none) > .btn-secondary')
      .each((_, rawEl) => {$(rawEl).closest('.col').remove();});
  }

  async setup(args: ModuleSetupArgs, cfg: Config) {
    const postsCfg = cfg.post || {};
    if (!R.prop('silent', args || {})) {
      debugLog('setupPosts', postsCfg);
    }
    if (postsCfg.openInNewTab) {
      $('.post-title a').prop('target', '_blank');
      $('.post-actions a > i.fa-comment-dots').parent().prop('target', '_blank');
    }
    if (postsCfg.hidePostsFromGuilds) {
      const toHide = $('#posts > .card').filter(this.isPostFromHiddenGuild(postsCfg));
      if (postsCfg.fullyHidePostsFromGuilds) {
        toHide.hide();
        this.somePostsFullyHiddenCb?.();
      } else {
        toHide.addClass(semiHiddenPostFromGuildCls);
      }
    }
    if (postsCfg.blurNsfwThumbnails) {
      $('body').addClass(blurNsfwThumbnailsCls);
    }
    if (postsCfg.hideAlreadyJoinedGuildsInDiscovery && this.isOnDiscoverPage()) {
      this.hideAlreadyJoinedGuildTiles();
      this.somePostsFullyHiddenCb?.();
    }
    if (postsCfg.improvedTableStyles) {
      $(`.${boxPostTextCls}, #post-body`).addClass(improvedTableCls);
    }
    if (postsCfg.lessAggressivePatronBadges) {
      $('.post-meta .badge, .user-info .badge').filter((_, el) => $(el).text().includes('Patron')).addClass(nonAggressiveCls);
    }
  };

  async onContentChange(args: ModuleSetupArgs, cfg: Config): Promise<unknown> {
    return this.setup(args, cfg);
  }
}
