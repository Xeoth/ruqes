import {voteAnimCls} from 'common/styles';
import {debugLog, printError} from 'common/common';
import {Config} from 'common/config';
import {ModuleSetupArgs} from 'common/modules';
import {RuqESModule} from 'common/RuqESModule';

export class VotingModule extends RuqESModule {
  async setup(args: ModuleSetupArgs, cfg: Config) {
    const votingCfg = cfg.voting || {};
    if (!R.prop('silent', args || {})) {
      debugLog('setupVoting', votingCfg);
    }
    if (votingCfg.bigVoteArrowsOnMobile) {
      $('.voting.d-md-none').css('margin-bottom', '-1rem').css('margin-top', '-0.4rem')
        .find('span.arrow-mobile-up, span.arrow-mobile-down').css('height', '3rem').css('font-size', '1.5rem')
        .find('i').css('font-size', '1.5rem')
      ;
    }
    if (votingCfg.clickEffect) {
      $('span.arrow-mobile-up, span.arrow-mobile-down').click((evt) => {
        const evtTargetEl = $(evt.target);
        let el = evtTargetEl;
        if (el.prop('tagName').toLowerCase() === 'i') {
          el = el.parent();
        }
        if (el.prop('tagName').toLowerCase() !== 'span') {
          printError('clickEffect: failed to locate voting <span>');
          return true;
        }
        debugLog('voting - clickEffect - click handler', 'el =', el, '; evtTargetEl = ', evtTargetEl, '; evt =', evt);
        const animEl = $('<i>')
          .addClass(el.prop('class').includes('up') ? 'fa-arrow-alt-up' : 'fa-arrow-alt-down')
          .addClass('fas position-absolute')
          .addClass(voteAnimCls);
        el.prepend(animEl);
        el.css({position: 'relative'});
        setTimeout(() => animEl.remove(), 1000);
        return true;
      });
    }
    return Promise.resolve();
  };

  async onContentChange(args: ModuleSetupArgs, cfg: Config): Promise<unknown> {
    return this.setup(args, cfg);
  }
}
