import {Config} from 'common/config';
import {debugLog} from 'common/common';
import {boxEmbedCls, boxEmptyCls, boxImgCls, expandBoxCls, expandBoxOpenedCls, expandBtnCls} from 'common/styles';
import {getPosts} from 'common/selectors';
import {handleTextOfPost, linkHandlers} from 'modules/processors';
import {handleModulesAfterContentChange, ModuleSetupArgs} from 'common/modules';
import {RuqESModule} from '../common/RuqESModule';

export class ExpandoButtonModule extends RuqESModule {
  private createExpandButton = (cfg: Config) => $('<a>')
    .text(cfg.expandoButton.textClosed)
    .addClass(expandBtnCls)
    .addClass(`${expandBtnCls}--style-${cfg.expandoButton.style}`)
    .addClass('btn')
    .addClass('btn-secondary')
    .prop('role', 'button')
    .prop('href', 'javascript:void 0')
  ;

  private createBox = () => $('<div>')
    .addClass(expandBoxCls)
    .addClass(boxEmptyCls)
  ;

  static insertWrappedElementIntoBox = (box: JQuery, element: JQuery, wrapperClass: string = boxEmbedCls) => {
    const wrapped = $('<div>').addClass(wrapperClass).html(element);
    ExpandoButtonModule.insertRawElementIntoBox(box, wrapped);
    handleModulesAfterContentChange();
  };

  static insertRawElementIntoBox = (box: JQuery, element: JQuery) => {
    box.append(element);
  };

  static setupResizing = (el: JQuery) => {
    interface ResizingState {
      resizing: boolean;
      startPoint: [number, number] | null;
      startValue: number;
      value: number;
    }

    const state: ResizingState = {
      resizing: false,
      startPoint: null,
      startValue: 100,
      value: 100,
    };
    el.mousedown(e => {
      state.resizing = true;
      state.startPoint = [e.clientX, e.clientY];
      state.startValue = state.value;
    });
    const doc = $(document);
    doc.mouseup(e => {
      state.resizing = false;
      state.startPoint = null;
    });
    doc.mousemove(e => {
      if (!state.resizing || !state.startPoint) {
        return;
      }
      const nx = e.clientX;
      const ny = e.clientY;
      const [ox, oy] = state.startPoint;
      const dx = nx - ox;
      const dy = ny - oy;
      const dist = dx + dy;
      state.value = R.clamp(20, 500, state.startValue + dist / 5);
      const percVal = `${state.value}%`;
      el.css({maxWidth: percVal, width: percVal});
    });
  };

  static insertImageIntoBox = (box: JQuery, link: string, cfg: Config) => {
    const el = $('<img>').prop('src', link).prop('draggable', false).addClass(boxImgCls);
    if (cfg?.expandoButton?.resize) {
      ExpandoButtonModule.setupResizing(el);
    }
    ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
  };

  private genButtonClickHandler = (el: JQuery<HTMLElement>, link: string, postUrl: string, cfg: Config) => (evt: JQuery.Event) => {
    const box = el.find(`.${expandBoxCls}`);
    const boxOpened = box.hasClass(expandBoxOpenedCls);
    const btn = $((evt as any).target);
    if (boxOpened) {
      btn.text(cfg.expandoButton.textClosed);
      box.removeClass(expandBoxOpenedCls);
      return;
    }
    btn.text(cfg.expandoButton.textOpened);
    box.addClass(expandBoxOpenedCls);
    if (box.hasClass(boxEmptyCls)) {
      box.removeClass(boxEmptyCls);
      handleTextOfPost(postUrl, box, cfg);
      const processed = linkHandlers.reduce((acc, x) => acc || x(link, box, cfg), false);
      if (!processed) {
        const unsupportedEl = $('<div>')
          .append('Unsupported link type: ')
          .append($('<code>').html($('<a>').prop('href', link).text(link)))
          .append('.');
        ExpandoButtonModule.insertWrappedElementIntoBox(box, unsupportedEl);
      }
    }
  };

  private processPostItem = (cfg: Config) => (_: unknown, domEl: HTMLElement) => {
    const el = $(domEl);
    const link = el.find('.card-header a').prop('href');
    const postUrl = el.find('.card-block .post-title a').prop('href');
    if (!link) {
      return;
    }
    el.append(this.createBox());
    const btn = this.createExpandButton(cfg);
    btn.click(this.genButtonClickHandler(el, link, postUrl, cfg));
    const actions = el.find('.post-actions > ul');
    if (cfg.expandoButton.alignRight) {
      actions.append(btn);
    } else {
      actions.prepend(btn);
    }
  };

  async setup(args: ModuleSetupArgs, cfg: Config) {
    if (!cfg?.expandoButton?.enabled) {
      return;
    }
    if (!R.prop('silent', args || {})) {
      debugLog('setupExpandoButton');
    }
    getPosts().filter((_, el) => $(el).find(`.${expandBtnCls}`).length === 0).each(this.processPostItem(cfg));
  };

  async onContentChange(args: ModuleSetupArgs, cfg: Config): Promise<unknown> {
    return this.setup(args, cfg);
  }
}
