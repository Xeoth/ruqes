import {createTextLoader, debugLog, xhr} from 'common/common';
import {boxPostTextCls, errorTextCls, postLoadingCls, boxPostCommentsCls, btnPrimaryCls} from 'common/styles';
import {Config} from 'common/config';
import {ExpandoButtonModule} from 'modules/ExpandoButtonModule';

const isDirectImageLink = (x: string) => /\.(?:jpg|jpeg|png|gif|svg|webp)(?:\?.*)?$|^https:\/\/i.ruqqus.com\//i.test(x);
const handleDirectImageLink: LinkProcessor = (link, box, cfg) => {
  if (!isDirectImageLink(link)) {
    return false;
  }
  ExpandoButtonModule.insertImageIntoBox(box, link, cfg);
  return true;
}

const imgurGifvLinkRegexGen = () => /https:\/\/i\.imgur\.com\/(\w+)\.(gifv|mp4)/g;
const isImgurGifvLink = (x: string) => imgurGifvLinkRegexGen().test(x);
const handleImgurGifv: LinkProcessor = (link, box) => {
  if (!isImgurGifvLink(link)) {
    return false;
  }
  const matches = [...link.matchAll(imgurGifvLinkRegexGen())];
  const id = R.path([0, 1], matches);
  const el = $(`<div class="handleImgurGifv"></div><blockquote class="imgur-embed-pub" lang="en" data-id="${id}"><a href="//imgur.com/${id}">?</a></blockquote><script async src="//s.imgur.com/min/embed.js" charset="utf-8"></script></div>`);
  ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
  return true;
};


const brokenImgurRegexGen = () => /^https:\/\/(?:m\.)?imgur\.com\/.*?([a-zA-Z0-9]+)$/gm;
const isBrokenImgurLink = (x: string) => brokenImgurRegexGen().test(x) && !x.includes('gallery');
const handleBrokenImgur: LinkProcessor = (link, box, cfg) => {
  if (!isBrokenImgurLink(link)) {
    return false;
  }
  xhr({
    method: 'GET',
    url: link,
    onload: resp => {
      const parsed = $(resp.response);
      const imgContainer = parsed.find('.post-image-container');
      const imgId = imgContainer.prop('id');
      const src = `https://i.imgur.com/${imgId}.png`;
      ExpandoButtonModule.insertImageIntoBox(box, src, cfg);
    },
  });
  return true;
};

const writeErrorToExpandButtonBox = (box: JQuery, errorText: string, retryCb: (() => void) | null = null) => {
  const errorEl = $('<div>')
    .addClass(errorTextCls)
    .append($('<span>').text(errorText))
  ;
  if (retryCb) {
    const retryButton = $('<button>')
      .text('Retry')
      .addClass('btn')
      .addClass('btn-primary')
      .addClass('ml-1')
      .click(() => {
        box.find(`.${errorTextCls}`).remove();
        retryCb();
      });
    errorEl.append(retryButton);
  }
  box.prepend(errorEl);
};
const onFetchingPostFailed = (postUrl: string, box: JQuery, cfg: Config) => (resp: GM.Response<unknown>) => {
  console.error('Failed to fetch a post.', resp);
  box.find(`.${postLoadingCls}`).remove();
  writeErrorToExpandButtonBox(
    box,
    `Failed to fetch the post. Error ${resp.status}: ${resp.statusText}`,
    () => handleTextOfPost(postUrl, box, cfg)
  );
};
export const handleTextOfPost = (postUrl: string, box: JQuery, cfg: Config) => {
  box.append(createTextLoader('Fetching post', postLoadingCls));
  xhr({
    method: 'GET',
    url: postUrl,
    onload: resp => {
      if (resp.status !== 200) {
        return onFetchingPostFailed(postUrl, box, cfg)(resp);
      }
      const parsed = $(resp.response);
      const post = parsed.find('#post-body').children().filter(':not(.embed-responsive)');
      box.find(`.${postLoadingCls}`).remove();
      const emptyPost = !post.length || post.text().trim() === '';
      const comments = cfg.expandoButton.showComments ? parsed.find('.comment-section:not(.text-center) > .comment') : $();
      const emptyComments = comments.length === 0;
      if (emptyPost && emptyComments) { return; }
      const wrappedComments = $('<div>').addClass(boxPostCommentsCls).append(comments).hide();
      const showCommentsButton = $('<a>').addClass('btn btn-secondary').text('Toggle comments').click(evt => {
        const el = $(evt.target);
        el.parent().find(`.${boxPostCommentsCls}`).toggle();
      });
      const combined = $('<div>').append(post);
      if (!emptyComments) { combined.append(showCommentsButton).append(wrappedComments); }
      ExpandoButtonModule.insertWrappedElementIntoBox(box, combined, boxPostTextCls);
    },
    onerror: onFetchingPostFailed(postUrl, box, cfg),
  });
};


const isRuqqusPost = (x: string) => /^https:\/\/ruqqus\.com\/post\/.*$/.test(x);
const handleRuqqusPost: LinkProcessor = (link, box) => {
  if (!isRuqqusPost(link)) {
    return false;
  }
  // already handled by handleTextOfPost
  return true;
};


const imgurAlbumPrefix = 'https://imgur.com/a/';
const isImgurAlbum = (x: string) => x.startsWith(imgurAlbumPrefix);
const handleImgurAlbum: LinkProcessor = (link, box) => {
  if (!isImgurAlbum(link)) {
    return false;
  }
  const albumId = R.drop(imgurAlbumPrefix.length, link);
  const el = $(`<div class="handleImgurAlbum embed-responsive"></div><blockquote class="imgur-embed-pub" lang="en" data-id="a/${albumId}"><a href="//imgur.com/a/${albumId}">?</a></blockquote><script async src="//s.imgur.com/min/embed.js" charset="utf-8"></script></div>`);
  ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
  return true;
};


const genYouTubeVideoRegex = () => /^https:\/\/(?:www|m)\.youtube\.com\/watch\?v=([\w-]+).*$|^https:\/\/youtu\.be\/([\w-]+)$/g;
const isYouTubeVideo = (x: string) => genYouTubeVideoRegex().test(x);
const handleYouTubeVideo: LinkProcessor = (link, box) => {
  if (!isYouTubeVideo(link)) {
    return false;
  }
  const matches = [...link.matchAll(genYouTubeVideoRegex())];
  const id = R.path([0, 1], matches) || R.path([0, 2], matches);
  if (!id) {
    console.log('failed to get YouTube video id', link);
    return false;
  }
  const el = $(`<div class="handleYouTubeVideo embed-responsive embed-responsive-16by9"><iframe width="560" height="315" src="https://www.youtube.com/embed/${id}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>`)
  ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
  return true;
}


const genBitChuteVideoRegex = () => /^https:\/\/www\.bitchute\.com\/video\/(\w+).*$/g;
const isBitChuteVideo = (x: string) => genBitChuteVideoRegex().test(x);
const handleBitChuteVideo: LinkProcessor = (link, box) => {
  if (!isBitChuteVideo(link)) {
    return false;
  }
  const matches = [...link.matchAll(genBitChuteVideoRegex())];
  const id = R.path([0, 1], matches);
  if (!id) {
    console.log('failed to get BitChute video id', link);
    return false;
  }
  const el = $(`<div class="handleBitChuteVideo embed-responsive embed-responsive-16by9"><iframe width="640" height="360" scrolling="no" frameborder="0" style="border: none;" src="https://www.bitchute.com/embed/${id}/"></iframe></div>`)
  ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
  return true;
};

const genRedGifsVideoRegex = () => /^https:\/\/redgifs\.com\/watch\/([\w_\d]+).*$/g;
const isRedGifsVideo = (x: string) => genRedGifsVideoRegex().test(x);
const handleRedGifsVideo: LinkProcessor = (link, box) => {
  if (!isRedGifsVideo(link)) {
    return false;
  }
  const matches = [...link.matchAll(genRedGifsVideoRegex())];
  const id = R.path([0, 1], matches);
  if (!id) {
    console.log('failed to get RedGifs video id', link);
    return false;
  }
  const el = $(`<div class="handleRedGifsVideo embed-responsive embed-responsive-16by9"><iframe src='https://redgifs.com/ifr/${id}' frameborder='0' scrolling='no' allowfullscreen width='640' height='360'></iframe></div>`)
  ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
  return true;
};

const genGfycatVideoRegex = () => /^https:\/\/gfycat\.com\/([\w_\d]+).*$/g;
const isGfycatVideo = (x: string) => genGfycatVideoRegex().test(x);
const handleGfycatVideo: LinkProcessor = (link, box) => {
  if (!isGfycatVideo(link)) {
    return false;
  }
  const matches = [...link.matchAll(genGfycatVideoRegex())];
  const id = R.path([0, 1], matches);
  if (!id) {
    console.log('failed to get Gfycat video id', link);
    return false;
  }
  const el = $(`<div class="handleGfycatVideo embed-responsive embed-responsive-16by9"><iframe src='https://gfycat.com/ifr/${id}' frameborder='0' scrolling='no' allowfullscreen width='640' height='492'></iframe></div>`)
  ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
  return true;
};

const gifDeliveryNetworkRegexGen = () => /^https:\/\/(?:www\.)gifdeliverynetwork\.com\/([\w_\d]+).*$/gm;
const isGifDeliveryNetworkLink = (x: string) => gifDeliveryNetworkRegexGen().test(x);
const handleGifDeliveryNetwork: LinkProcessor = (link, box) => {
  if (!isGifDeliveryNetworkLink(link)) {
    return false;
  }
  const matches = [...link.matchAll(gifDeliveryNetworkRegexGen())];
  const id = R.path([0, 1], matches);
  if (!id) {
    console.log('failed to get GifDeliveryNetwork video id', link);
    return false;
  }
  const el = $(`<div class="handleGfycatVideo embed-responsive embed-responsive-16by9"><iframe src='https://gfycat.com/ifr/${id}' frameborder='0' scrolling='no' allowfullscreen width='640' height='492'></iframe></div>`)
  ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
  return true;
};

const imgflipLinkRegex = () => /^https:\/\/imgflip\.com\/i\/.*?([a-zA-Z0-9]+)$/gm;
const isImgflipLink = (x: string) => imgflipLinkRegex().test(x);
const handleImgflipLink: LinkProcessor = (link, box, cfg) => {
  if (!isImgflipLink(link)) {
    return false;
  }
  const matches = [...link.matchAll(imgflipLinkRegex())];
  const id = R.path([0, 1], matches);
  const src = `https://i.imgflip.com/${id}.jpg`;
  ExpandoButtonModule.insertImageIntoBox(box, src, cfg);
  return true;
}

const ibbcoLinkRegex = () => /^https:\/\/ibb.co\//gm;
const isIbbcoLink = (x: string) => ibbcoLinkRegex().test(x);
const handleIbbcoLink: LinkProcessor = (link, box, cfg) => {
  if (!isIbbcoLink(link)) {
    return false;
  }
  xhr({
    method: 'GET',
    url: link,
    onload: resp => {
      const parsed = $(resp.response);
      const img = parsed.find('#image-viewer-container img');
      const imgUrl = img.prop('src');
      ExpandoButtonModule.insertImageIntoBox(box, imgUrl, cfg);
    },
  });
  return true;
};


const twitterLinkRegex = () => /^https:\/\/(?:mobile\.)?twitter\.com\/([\w0-9]+)\/status\/(\d+)$/gm;
const isTwitterLink = (x: string) => twitterLinkRegex().test(x);
const handleTwitterLink: LinkProcessor = (link, box) => {
  if (!isTwitterLink(link)) {
    return false;
  }
  xhr({
    method: 'GET',
    url: `https://publish.twitter.com/oembed?url=${encodeURIComponent(link)}`,
    onload: resp => {
      const data = JSON.parse(resp.response);
      debugLog('handleTwitterLink', data);
      const parsed = $(data.html);
      // TODO: option to remove script
      const el = $('<div>').css({margin: '-10px 0'}).html(parsed);
      ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
    },
  });
  return true;
};


// TODO: lbry?

export type LinkProcessor = (link: string, box: JQuery, cfg: Config) => boolean;

export const linkHandlers: LinkProcessor[] = [
  handleDirectImageLink,
  handleRuqqusPost,
  handleImgurAlbum,
  handleBrokenImgur,
  handleYouTubeVideo,
  handleBitChuteVideo,
  handleRedGifsVideo,
  handleGfycatVideo,
  handleGifDeliveryNetwork,
  handleImgurGifv,
  handleImgflipLink,
  handleIbbcoLink,
  handleTwitterLink,
];
