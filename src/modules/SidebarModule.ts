import {getRightSidebarEl} from 'common/selectors';
import {sidebarButtonIconCls, sidebarClosedCls} from 'common/styles';
import {$c, debugLog, setClass} from 'common/common';
import {Config} from 'common/config';
import {RuqESModule} from 'common/RuqESModule';
import {ModuleSetupArgs} from '../common/modules';

export class SidebarModule extends RuqESModule {
  toggleSidebar() {
    const ico = $c(sidebarButtonIconCls);
    ico.toggleClass('opened');
    const opened = ico.hasClass('opened');
    setClass(getRightSidebarEl(), sidebarClosedCls, !opened);
  };

  async setup(args: ModuleSetupArgs, cfg: Config) {
    const sidebarCfg = cfg.sidebar || {};
    debugLog('setupSidebar', sidebarCfg);
    if (sidebarCfg.rightButton) {
      const iconEl = $('<span>').addClass(sidebarButtonIconCls).addClass('opened');
      const btnEl = $('<a>').addClass('btn').css({paddingTop: 0}).html(iconEl).click(this.toggleSidebar);
      getRightSidebarEl().prepend(btnEl);
      if (sidebarCfg.autoHideRight) { this.toggleSidebar(); }
    }
  }
}
