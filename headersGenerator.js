const gen = data => {
    return {
        name: 'RuqES - ruqqus.com',
        version: data.version,
        namespace: 'enefi',
        match: [
            'https://ruqqus.com/*',
            'https://linode.ruqqus.com/*',
            'http://ruqqus.localhost:8000/*',
        ],
        grant: [
            'GM.xmlHttpRequest',
            'GM.info',
            'GM.getValue',
            'GM.setValue',
        ],
        author: 'enefi',
        description: '"Ruqqus Enhancement Suite" brings infinite scroll, expando button and more. Recommended to be used with Violentmonkey. For more details see https://ruqqus.com/post/ldx/what-is-ruqes',
        require: [
            'https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js',
            'https://cdn.jsdelivr.net/npm/ramda@0.27.0/dist/ramda.min.js',
        ],
        homepageURL: 'https://ruqqus.com/post/ldx/what-is-ruqes',
        supportURL: 'https://ruqqus.com/post/p04/bug-reports-and-other-issues',
    };
};

module.exports = gen;
