// ==UserScript==
// @name        RuqES - ruqqus.com
// @version     0.12.0
// @author      enefi
// @description "Ruqqus Enhancement Suite" brings infinite scroll, expando button and more. Recommended to be used with Violentmonkey. For more details see https://ruqqus.com/post/ldx/what-is-ruqes
// @supportURL  https://ruqqus.com/post/p04/bug-reports-and-other-issues
// @match       https://ruqqus.com/*
// @match       https://linode.ruqqus.com/*
// @match       http://ruqqus.localhost:8000/*
// @namespace   enefi
// @grant       GM.xmlHttpRequest
// @grant       GM.info
// @grant       GM.getValue
// @grant       GM.setValue
// @require     https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js
// @require     https://cdn.jsdelivr.net/npm/ramda@0.27.0/dist/ramda.min.js
// @homepageURL https://ruqqus.com/post/ldx/what-is-ruqes
// ==/UserScript==

/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 8);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.dropWhileString = exports.preventDefaults = exports.xhr = exports.isDarkTheme = exports.createTextLoader = exports.createRuqesMark = exports.createLoadingDots = exports.setClass = exports.$i = exports.$c = exports.printError = exports.printLog = exports.debugLog = exports.enabledDebug = exports.isDebugEnabled = exports.scriptVersion = exports.scriptInfo = exports.logPrefix = void 0;
var styles_1 = __webpack_require__(1);
var logo_svg_1 = __importDefault(__webpack_require__(4));
exports.logPrefix = '[RuqES]';
exports.scriptInfo = GM.info.script;
exports.scriptVersion = exports.scriptInfo.version;
var debugEnabled = false;
exports.isDebugEnabled = function () { return debugEnabled; };
exports.enabledDebug = function () { return debugEnabled = true; };
exports.debugLog = function () {
    var xs = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        xs[_i] = arguments[_i];
    }
    return exports.isDebugEnabled() && console.log.apply(console, __spread([exports.logPrefix], xs));
};
exports.printLog = function () {
    var xs = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        xs[_i] = arguments[_i];
    }
    return console.log.apply(console, __spread([exports.logPrefix], xs));
};
exports.printError = function () {
    var xs = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        xs[_i] = arguments[_i];
    }
    return console.error.apply(console, __spread([exports.logPrefix], xs));
};
exports.$c = function (cls) { return $("." + cls); };
exports.$i = function (cls, parent) { return $("#" + cls, parent); };
exports.setClass = function (el, cls, value) {
    if (value) {
        el.addClass(cls);
    }
    else {
        el.removeClass(cls);
    }
    return el;
};
exports.createLoadingDots = function () {
    var container = $('<span>').addClass(styles_1.loadingDotsCls);
    R.range(0, 3).map(function () { return $('<i>.</i>'); }).forEach(function (x) { return container.append(x); });
    return container;
};
exports.createRuqesMark = function () { return $('<span>').html(logo_svg_1.default).addClass(styles_1.ruqesMarkCls); };
exports.createTextLoader = function (text, cls) {
    return $('<div>')
        .addClass(styles_1.textLoaderCls)
        .addClass(cls)
        .append(exports.createRuqesMark())
        .append($('<span>').text(text).css('margin-left', '0.33em'))
        .append(exports.createLoadingDots());
};
exports.isDarkTheme = function () { return $('#dark-switch').hasClass('fa-toggle-on'); };
exports.xhr = function (cfg) { return GM.xmlHttpRequest(cfg); };
exports.preventDefaults = function (evt) {
    evt.preventDefault();
    evt.stopPropagation();
};
exports.dropWhileString = function (pred, y) { return R.dropWhile(pred, y); };


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.setupStyles = exports.nonAggressiveCls = exports.improvedTableCls = exports.infiniteScrollCoolDownMarkerCls = exports.widthAutoCls = exports.blurNsfwThumbnailsCls = exports.uploadImageToImgbbButtonLoadingCls = exports.uploadImageToImgbbButtonHighlightedCls = exports.uploadImageToImgbbButtonCls = exports.loadTitleButtonLoadingCls = exports.loadTitleButtonCls = exports.btnDangerCls = exports.btnPrimaryCls = exports.sidebarClosedCls = exports.sidebarButtonIconCls = exports.semiHiddenPostFromGuildCls = exports.voteAnimCls = exports.blurCls = exports.blurAnimatedCls = exports.formCls = exports.settingsExpandoButtonActionCharacterCls = exports.settingsLogoCls = exports.settingsCaptionCls = exports.settingsModalVisibleCls = exports.settingsModalCls = exports.settingsBackdropCls = exports.creatorNameCls = exports.creatorMarkCls = exports.ruqesMarkCls = exports.pageLoadingErrorCls = exports.errorTextCls = exports.textLoaderCls = exports.codeBlockCls = exports.loadingDotsCls = exports.postLoadingCls = exports.pageLoadingCls = exports.boxPostCommentsCls = exports.boxPostTextCls = exports.boxEmbedCls = exports.boxImgCls = exports.expandBtnCls = exports.boxEmptyCls = exports.expandBoxOpenedCls = exports.expandBoxCls = exports.genClsName = exports.clsPrefix = void 0;
var microtip_css_1 = __importDefault(__webpack_require__(9));
var addStyle = function (style) {
    style = style instanceof Array ? style.join('\n') : style;
    $('head').append($('<style type="text/css">' + style + '</style>'));
};
exports.clsPrefix = 'RuqES-by-enefi--';
exports.genClsName = function (x) { return exports.clsPrefix + x; };
exports.expandBoxCls = exports.genClsName('box');
exports.expandBoxOpenedCls = exports.genClsName('box-opened');
exports.boxEmptyCls = exports.genClsName('box-empty');
exports.expandBtnCls = exports.genClsName('expando-button');
exports.boxImgCls = exports.genClsName('box-img');
exports.boxEmbedCls = exports.genClsName('box-embed');
exports.boxPostTextCls = exports.genClsName('box-post-text');
exports.boxPostCommentsCls = exports.genClsName('box-post-comments');
exports.pageLoadingCls = exports.genClsName('page-loader');
exports.postLoadingCls = exports.genClsName('post-loader');
exports.loadingDotsCls = exports.genClsName('loading-dots');
exports.codeBlockCls = exports.genClsName('code-block');
exports.textLoaderCls = exports.genClsName('text-loader');
exports.errorTextCls = exports.genClsName('error-text');
exports.pageLoadingErrorCls = exports.genClsName('page-loading-error');
exports.ruqesMarkCls = exports.genClsName('ruqes-mark');
exports.creatorMarkCls = exports.genClsName('creator-mark');
exports.creatorNameCls = exports.genClsName('creator-name');
exports.settingsBackdropCls = exports.genClsName('settings-backdrop');
exports.settingsModalCls = exports.genClsName('settings-modal');
exports.settingsModalVisibleCls = exports.genClsName('settings-modal-shown');
exports.settingsCaptionCls = exports.genClsName('settings-caption');
exports.settingsLogoCls = exports.genClsName('settings-logo');
exports.settingsExpandoButtonActionCharacterCls = exports.genClsName('settings-expando-action-char');
exports.formCls = exports.genClsName('form');
exports.blurAnimatedCls = exports.genClsName('blur-anim');
exports.blurCls = exports.genClsName('blur');
exports.voteAnimCls = exports.genClsName('vote-anim');
exports.semiHiddenPostFromGuildCls = exports.genClsName('semi-hidden-post-from-guild');
exports.sidebarButtonIconCls = exports.genClsName('sidebar-button-icon');
exports.sidebarClosedCls = exports.genClsName('sidebar-closed');
exports.btnPrimaryCls = exports.genClsName('btn-primary');
exports.btnDangerCls = exports.genClsName('btn-danger');
exports.loadTitleButtonCls = exports.genClsName('post-load-title-button');
exports.loadTitleButtonLoadingCls = exports.genClsName('post-load-title-button-loading');
exports.uploadImageToImgbbButtonCls = exports.genClsName('post-upload-imgbb-button');
exports.uploadImageToImgbbButtonHighlightedCls = exports.genClsName('post-upload-imgbb-button-highlighted');
exports.uploadImageToImgbbButtonLoadingCls = exports.genClsName('post-upload-imgbb-button-loading');
exports.blurNsfwThumbnailsCls = exports.genClsName('blur-nsfw-thumbnails');
exports.widthAutoCls = exports.genClsName('width-auto');
exports.infiniteScrollCoolDownMarkerCls = exports.genClsName('inifinite-scroll-cooldown-marker');
exports.improvedTableCls = exports.genClsName('improved-table');
exports.nonAggressiveCls = exports.genClsName('non-aggressive');
exports.setupStyles = function (isDarkTheme) {
    var primaryColor = 'rgba(128, 90, 213, 1)';
    var ruqesColor = '#800080';
    var containerBorderColor = isDarkTheme ? '#303030' : '#E2E8F0';
    var containerBackgroundColor = isDarkTheme ? '#181818' : '#D2D8E0';
    var containerBackgroundColorSecondary = isDarkTheme ? '#232323' : '#c8ced6';
    var textColor = isDarkTheme ? 'white' : 'black';
    var arrowVoteColor = isDarkTheme ? '#303030' : '#cbd5e0';
    var upvoteColor = '#805ad5';
    var downvoteColor = '#38b2ac';
    addStyle(microtip_css_1.default);
    addStyle("\n." + exports.widthAutoCls + " { width: auto; }\n  \n." + exports.expandBtnCls + " {\n  margin: -0.4em 1em -0.4em 0;\n  padding: 0;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n}\n\n." + exports.expandBtnCls + ":last-child {\n  margin: -0.4em 0 -0.4em 1em;\n}\n\n." + exports.expandBtnCls + "--style-bigWide {\n  width: 3.25em;\n  height: 2.5em;\n}\n." + exports.expandBtnCls + "--style-big {\n  width: 2.5em;\n  height: 2.5em;\n}\n." + exports.expandBtnCls + "--style-medium {\n  width: 2em;\n  height: 2em;\n  font-size: 90%;\n}\n." + exports.expandBtnCls + "--style-small {\n  width: 1.5em;\n  height: auto;\n  margin: 0 0.5em 0 0;\n  border: 0;\n  background: none;\n}\n\n." + exports.expandBoxCls + " {\n  display: none;\n  max-width: 100%;\n  border: 1px solid " + containerBorderColor + ";\n  margin-top: 1em;\n  padding-left: 0;\n  padding-right: 0;\n}\n\n." + exports.expandBoxOpenedCls + " {\n  display: flex;\n  flex-direction: column;\n}\n\nimg." + exports.boxImgCls + " {\n  max-width: 100%;\n}\n\n.stretched-link::after { display: none; }\n\n." + exports.boxEmbedCls + " {\n  border-left: 1px solid " + containerBorderColor + ";\n  order: 1;\n}\n\n." + exports.boxPostTextCls + " {\n  border-left: 1px solid " + containerBorderColor + ";\n  order: 0;\n  padding: 5px;\n}\n\n." + exports.boxPostTextCls + " p:last-child {\n  margin-bottom: 0;\n}\n\n." + exports.boxPostCommentsCls + " {\n}\n\n." + exports.boxPostCommentsCls + " .comment-actions > ul > li.arrow-up::before,\n." + exports.boxPostCommentsCls + " .comment-actions > ul > li.arrow-down::before {\n  color: " + arrowVoteColor + ";\n}\n\n." + exports.boxPostCommentsCls + " .comment-actions.upvoted > ul > li.arrow-up::before {\n  color: " + upvoteColor + ";\n}\n\n." + exports.boxPostCommentsCls + " .comment-actions.downvoted > ul > li.arrow-down::before {\n  color: " + downvoteColor + ";\n}\n\n@keyframes ruqesLoadingDots {\n  0% {\n    transform: translate(0, 0) scale(1);\n    color: " + primaryColor + "\n  }\n  25% {\n    transform: translate(0, -0.2em) scale(1.33);\n  }\n  50% {\n    transform: translate(0, 0) scale(1);\n  }\n  55% {\n  }\n  65% {\n    color: " + textColor + "\n  }\n}\n\n." + exports.loadingDotsCls + " i {\n  animation-name: ruqesLoadingDots;\n  animation-duration: 2s;\n  animation-delay: 0s;\n  animation-iteration-count: infinite;\n  animation-timing-function: ease-in-out;\n  display: inline-block;\n}\n\n." + exports.loadingDotsCls + " i:nth-child(2) { animation-delay: 0.2s; }\n." + exports.loadingDotsCls + " i:nth-child(3) { animation-delay: 0.4s; }\n\n." + exports.codeBlockCls + " {\n  border: 1px solid " + containerBorderColor + ";\n  padding: 1em;\n  font-size: 66%;\n  background-color: lightgray;\n}\n\n." + exports.textLoaderCls + " {\n  font-weight: bold;\n}\n\n." + exports.pageLoadingCls + " {\n  margin: 1em 0 3em 0;\n  text-align: center;\n}\n\n." + exports.errorTextCls + " {\n  font-weight: bold;\n  color: red;\n}\n\n." + exports.pageLoadingErrorCls + " {\n  text-align: left;\n}\n\n." + exports.ruqesMarkCls + " svg {\n  width: 1.2em;\n  height: 1.2em;\n}\n\n." + exports.creatorMarkCls + " svg {\n  width: 1.2em;\n  height: 1.2em;\n}\n\n." + exports.creatorNameCls + " {\n  transition: color 0.2s;\n  color: " + (isDarkTheme ? '#bf40bf' : '#800080') + ";\n}\n." + exports.creatorNameCls + ":hover {\n  color: " + textColor + ";\n}\n\n." + exports.settingsBackdropCls + " {\n  position: fixed;\n  width: 100%;\n  height: 100%;\n  background-color: " + (containerBackgroundColor + "66") + ";\n  z-index: 10000;\n  top: 0;\n}\n\n." + exports.settingsModalCls + "::-webkit-scrollbar {\n    height: 5px;\n    width: 5px;\n    background: " + containerBorderColor + ";\n}\n." + exports.settingsModalCls + "::-webkit-scrollbar-thumb {\n    background: " + containerBackgroundColor + ";\n}\n." + exports.settingsModalCls + " {\n  position: fixed;\n  left: 50%;\n  top: 50%;\n  background-color: " + containerBorderColor + ";\n  border: 2px solid " + containerBackgroundColor + ";\n  border-radius: 5px;\n  padding: 0.5em;\n  transform: translate(-50%, -50%) scale(0.8);\n  min-width: 420px;\n  overflow-y: auto;\n  max-height: 100%;\n  opacity: 0;\n  transition: transform 0.2s, opacity 0.1s;\n  scrollbar-color: " + containerBackgroundColor + " " + containerBorderColor + ";\n  scrollbar-width: thin;\n}\n." + exports.settingsModalVisibleCls + " {\n  transform: translate(-50%, -50%) scale(1);\n  opacity: 1;\n}\n@media (max-width: 575.98px) {\n  ." + exports.settingsModalCls + " {\n    width: 100%;\n    min-width: auto;\n  }\n}\n\n." + exports.formCls + " input[type=\"checkbox\"] {\n  width: auto;\n  height: auto;\n  display: inline-block;\n  margin-right: 0.3em;\n}\n\n." + exports.formCls + " label {\n  margin-bottom: 0;\n}\n\n." + exports.formCls + " .form-group {\n  margin-bottom: 0;\n  display: flex;\n  align-items: center;\n}\n\n." + exports.blurAnimatedCls + " {\n  filter: blur(0);\n  transition: filter 0.5s ease-in-out;\n}\n\n." + exports.blurCls + " {\n  filter: blur(2px);\n}\n\n." + exports.settingsCaptionCls + " {\n  text-shadow: " + (isDarkTheme ? 'black' : '#999') + " 1px 0.5px 0px;\n}\n\n." + exports.settingsLogoCls + " {\n  display: flex;\n}\n." + exports.settingsLogoCls + " svg {\n  width: 2em;\n  height: 2em;\n  margin-right: 0.2em;\n}\n\n." + exports.settingsExpandoButtonActionCharacterCls + " input {\n  width: 4em;\n}\n." + exports.settingsExpandoButtonActionCharacterCls + " label {\n  width: 10em;\n}\n\n@keyframes ruqesVoteEffect {\n  0% {\n    transform: scale(1);\n    opacity: 0;\n  }\n  1% {\n    opacity: 1;\n  }\n  90% {\n    transform: scale(3);\n  }\n  100% {\n    transform: scale(3.1);\n    opacity: 0;\n  }\n}\n\n." + exports.voteAnimCls + " {\n  animation-name: ruqesVoteEffect;\n  animation-duration: 1s;\n  animation-delay: 0s;\n  animation-iteration-count: 1;\n  animation-timing-function: ease-in-out;\n  position: absolute !important;\n  left: 6%;\n  top: 20%;\n}\n\n." + exports.semiHiddenPostFromGuildCls + " {\n  opacity: 0.2;\n  transition: opacity 0.33s;\n  max-height: 1.5em;\n  background-color: " + containerBackgroundColor + ";\n  overflow: hidden;\n  padding-top: 0 !important;\n}\n." + exports.semiHiddenPostFromGuildCls + ":hover {\n  opacity: 1;\n  max-height: none;\n}\n\n." + exports.sidebarButtonIconCls + "::before {\n    cursor: pointer;\n    font-size: 1rem;\n    color: #777;\n    font-family: \"Font Awesome 5 Pro\";\n    font-weight: 900;\n    content: \"\uF348\";\n}\n\n." + exports.sidebarButtonIconCls + ".opened::before {\n    cursor: pointer;\n    font-size: 1rem;\n    color: #777;\n    font-family: \"Font Awesome 5 Pro\";\n    font-weight: 900;\n    content: \"\uF347\";\n}\n\n." + exports.sidebarClosedCls + " {\n    max-width: 3em;\n    padding: 0;\n}\n." + exports.sidebarClosedCls + " > :not(:first-child) {\n    display: none !important;\n}\n\n." + exports.btnPrimaryCls + " {\n    color: rgb(207, 207, 207) !important;\n    background-color: #6133c9;\n    border-color: #5c31bf;\n}\n\n." + exports.btnPrimaryCls + ":not(:disabled):not(.disabled):active, ." + exports.btnPrimaryCls + ":not(:disabled):not(.disabled).active, .show>." + exports.btnPrimaryCls + ".dropdown-toggle {\n    color: #fff !important;\n    background-color: #6133c9;\n    border-color: #5c31bf;\n}\n\n." + exports.btnDangerCls + " {\n    color: rgb(207, 207, 207) !important;\n    background-color: #e53e3e;\n    border-color: #e53e3e;\n}\n\n@keyframes ruqesSpinning {\n  0% {\n    transform: scale(1) rotate(0deg);\n  }\n  30% {\n    transform: scale(1.2) rotate(108deg);\n  }\n  60% {\n    transform: scale(0.8) rotate(216deg);\n  }\n  100% {\n    transform: scale(1) rotate(360deg);\n  }\n}\n\n." + exports.loadTitleButtonCls + " {\n  margin-top: -0.4em;\n}\n." + exports.loadTitleButtonCls + " svg {\n  width: 1.2em;\n  height: 1.2em;\n  margin-left: -0.3em;\n  margin-right: 0.2em;\n}\n." + exports.loadTitleButtonCls + "." + exports.loadTitleButtonLoadingCls + " svg {\n  animation: 1s infinite linear ruqesSpinning;\n}\n\n." + exports.uploadImageToImgbbButtonCls + " {\n  margin-top: -0.4em;\n  border: 1px dashed " + ruqesColor + ";\n}\n." + exports.uploadImageToImgbbButtonCls + " svg {\n  width: 1.2em;\n  height: 1.2em;\n  margin-left: -0.3em;\n  margin-right: 0.2em;\n}\n." + exports.uploadImageToImgbbButtonCls + "." + exports.uploadImageToImgbbButtonLoadingCls + " svg {\n  animation: 1s infinite linear ruqesSpinning;\n}\n\n." + exports.loadTitleButtonCls + " ~ ." + exports.uploadImageToImgbbButtonCls + " {\n  margin-left: 0.5em;\n}\n\n." + exports.uploadImageToImgbbButtonHighlightedCls + " {\n  border-style: solid;\n}\n\nbody." + exports.blurNsfwThumbnailsCls + " .card.nsfw .post-img {\n  filter: blur(7px) saturate(0.3);\n}\n\n." + exports.infiniteScrollCoolDownMarkerCls + " {\n  font-size: 130%;\n  margin-left: 1em;\n}\n\n." + exports.improvedTableCls + " td, ." + exports.improvedTableCls + " th {\n  border: 1px solid " + containerBorderColor + ";\n  padding: 0.2em 0.5em;\n}\n." + exports.improvedTableCls + " th {\n  background-color: " + containerBackgroundColorSecondary + ";\n}\n\n." + exports.nonAggressiveCls + " {\n  opacity: 0.33;\n  transition: opacity 0.2s;\n}\n." + exports.nonAggressiveCls + ":hover {\n  opacity: 1;\n}\n\n");
};


/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RuqESModule = void 0;
var RuqESModule = /** @class */ (function () {
    function RuqESModule() {
        this.somePostsFullyHiddenCb = null;
    }
    RuqESModule.prototype.onContentChange = function (args, cfg) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, Promise.resolve()];
            });
        });
    };
    RuqESModule.prototype.registerSomePostsFullyHiddenHandler = function (handler) {
        if (this.somePostsFullyHiddenCb) {
            throw new Error("somePostsFullyHidden handler already registered");
        }
        this.somePostsFullyHiddenCb = handler;
    };
    RuqESModule.prototype.onSomePostsFullyHidden = function (cfg) { };
    return RuqESModule;
}());
exports.RuqESModule = RuqESModule;


/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.getTopLevelContainersForBlurring = exports.getPostUrlInPostCreation = exports.getPostTitleInPostCreation = exports.getRightSidebarEl = exports.getPageLoadingMarker = exports.getNextPageLink = exports.getPagination = exports.getPosts = void 0;
var styles_1 = __webpack_require__(1);
var common_1 = __webpack_require__(0);
exports.getPosts = function () { return $('#posts > .card, .posts > .card'); };
exports.getPagination = function (from) { return $('ul.pagination', from).parent(); };
exports.getNextPageLink = function () { return exports.getPagination().find('a:contains(Next)'); };
exports.getPageLoadingMarker = function () { return $("." + styles_1.pageLoadingCls); };
exports.getRightSidebarEl = function () { return $('.sidebar:not(.sidebar-left)'); };
exports.getPostTitleInPostCreation = function () { return common_1.$i('post-title'); };
exports.getPostUrlInPostCreation = function () { return common_1.$i('post-URL'); };
exports.getTopLevelContainersForBlurring = function () { return $('#main-content-row, #submitform'); };


/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n<svg\n        xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n        xmlns:cc=\"http://creativecommons.org/ns#\"\n        xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n        xmlns:svg=\"http://www.w3.org/2000/svg\"\n        xmlns=\"http://www.w3.org/2000/svg\"\n        xmlns:sodipodi=\"http://sodipodi.sourceforge.net/DTD/sodipodi-0.dtd\"\n        xmlns:inkscape=\"http://www.inkscape.org/namespaces/inkscape\"\n        inkscape:version=\"1.0 (4035a4fb49, 2020-05-01)\"\n        sodipodi:docname=\"ruqes_logo_exported.svg\"\n        id=\"svg869\"\n        version=\"1.1\"\n        viewBox=\"0 0 255.99998 255.99998\"\n        height=\"255.99998mm\"\n        width=\"255.99998mm\">\n    <defs\n            id=\"defs863\" />\n    <sodipodi:namedview\n            inkscape:window-maximized=\"1\"\n            inkscape:window-y=\"30\"\n            inkscape:window-x=\"1920\"\n            inkscape:window-height=\"2082\"\n            inkscape:window-width=\"3834\"\n            fit-margin-bottom=\"0\"\n            fit-margin-right=\"0\"\n            fit-margin-left=\"0\"\n            fit-margin-top=\"0\"\n            showgrid=\"false\"\n            inkscape:document-rotation=\"0\"\n            inkscape:current-layer=\"text853\"\n            inkscape:document-units=\"mm\"\n            inkscape:cy=\"336.90225\"\n            inkscape:cx=\"777.44097\"\n            inkscape:zoom=\"0.7\"\n            inkscape:pageshadow=\"2\"\n            inkscape:pageopacity=\"0.0\"\n            borderopacity=\"1.0\"\n            bordercolor=\"#666666\"\n            pagecolor=\"#ffffff\"\n            id=\"base\" />\n    <metadata\n            id=\"metadata866\">\n        <rdf:RDF>\n            <cc:Work\n                    rdf:about=\"\">\n                <dc:format>image/svg+xml</dc:format>\n                <dc:type\n                        rdf:resource=\"http://purl.org/dc/dcmitype/StillImage\" />\n                <dc:title></dc:title>\n            </cc:Work>\n        </rdf:RDF>\n    </metadata>\n    <g\n            transform=\"translate(349.49404,269.36309)\"\n            id=\"layer1\"\n            inkscape:groupmode=\"layer\"\n            inkscape:label=\"Layer 1\">\n        <path\n                d=\"m -279.08335,-269.36309 c -2.74077,35.24592 -32.03738,62.81064 -68.01032,62.81064 h -2.18901 c -0.1388,1.78488 -0.21136,3.58881 -0.21136,5.41052 v 95.16266 c 0,37.794655 30.42651,68.221161 68.22116,68.221161 h 119.55756 c 37.79464,0 68.221154,-30.426506 68.221154,-68.221161 v -95.16266 c 0,-37.79465 -30.426514,-68.22116 -68.221154,-68.22116 z\"\n                style=\"fill:#800080;fill-opacity:1;stroke:#800080;stroke-width:0;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-opacity:1;paint-order:stroke fill markers\"\n                id=\"rect855\" />\n        <g\n                style=\"font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:219.931px;line-height:100%;font-family:'Fira Sans';-inkscape-font-specification:'Fira Sans';font-variant-ligatures:normal;font-variant-position:normal;font-variant-caps:normal;font-variant-numeric:normal;font-variant-alternates:normal;font-feature-settings:normal;text-indent:0;text-align:start;text-decoration:none;text-decoration-line:none;text-decoration-style:solid;text-decoration-color:#000000;letter-spacing:0px;word-spacing:0px;text-transform:none;writing-mode:lr-tb;direction:ltr;text-orientation:mixed;dominant-baseline:auto;baseline-shift:baseline;text-anchor:start;white-space:normal;shape-padding:0;vector-effect:none;fill:#ffffff;fill-opacity:1;stroke:none;stroke-width:0.264583px;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1\"\n                id=\"text853\"\n                aria-label=\"r+\">\n            <path\n                    id=\"path1505\"\n                    style=\"font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;font-size:219.931px;font-family:CostaRica;-inkscape-font-specification:CostaRica;fill:#ffffff;stroke-width:0.264583px\"\n                    d=\"m -220.88911,-156.30975 v -27.49137 h -61.80061 c -11.21648,0 -20.67351,9.45703 -20.67351,20.67351 v 89.291985 h 41.34703 v -82.474125 z\" />\n            <path\n                    id=\"path1507\"\n                    style=\"font-size:142.955px;baseline-shift:super;fill:#ffffff;stroke-width:0.264583px\"\n                    d=\"m -184.2268,-233.28552 v 26.87554 h -26.87554 v 17.72642 h 26.87554 v 26.87554 h 17.72642 v -26.87554 h 26.87554 v -17.72642 h -26.87554 v -26.87554 z\" />\n        </g>\n        <circle\n                style=\"fill:#800080;stroke:none;stroke-width:0.342901;stroke-linejoin:round;paint-order:stroke fill markers\"\n                id=\"circle857\"\n                cx=\"-109.7934\"\n                cy=\"-29.662453\"\n                r=\"16.299351\" />\n    </g>\n</svg>\n");

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.handleModulesAfterContentChange = exports.handleFirstSetupOfModules = exports.registerClassModule = void 0;
var config_1 = __webpack_require__(6);
var common_1 = __webpack_require__(0);
var modules = [];
exports.registerClassModule = function (cls) {
    cls.registerSomePostsFullyHiddenHandler(handleOnSomePostsFullyHidden);
    modules.push(cls);
};
var readConfigOrDefault = function () { return __awaiter(void 0, void 0, void 0, function () { return __generator(this, function (_a) {
    switch (_a.label) {
        case 0: return [4 /*yield*/, config_1.readConfig()];
        case 1: return [2 /*return*/, _a.sent()];
    }
}); }); };
exports.handleFirstSetupOfModules = function () { return __awaiter(void 0, void 0, void 0, function () {
    var cfg;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                common_1.debugLog('modules', 'handleFirstSetupOfModules');
                return [4 /*yield*/, readConfigOrDefault()];
            case 1:
                cfg = _a.sent();
                modules.forEach(function (m) { return m.setup({}, cfg); });
                return [2 /*return*/];
        }
    });
}); };
exports.handleModulesAfterContentChange = function () { return __awaiter(void 0, void 0, void 0, function () {
    var cfg;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                common_1.debugLog('modules', 'handleModulesAfterContentChange');
                return [4 /*yield*/, readConfigOrDefault()];
            case 1:
                cfg = _a.sent();
                modules.forEach(function (m) {
                    return m.onContentChange({ silent: true }, cfg);
                });
                return [2 /*return*/];
        }
    });
}); };
var handleOnSomePostsFullyHidden = function () { return __awaiter(void 0, void 0, void 0, function () {
    var cfg;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                common_1.debugLog('modules', 'handleOnSomePostsFullyHidden');
                return [4 /*yield*/, readConfigOrDefault()];
            case 1:
                cfg = _a.sent();
                modules.forEach(function (m) { return m.onSomePostsFullyHidden(cfg); });
                return [2 /*return*/];
        }
    });
}); };


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.writeConfig = exports.readConfig = exports.defaultConfig = exports.asExpandoButtonStyleOrDefault = exports.defaultExpandoButtonStyle = exports.expandoButtonStyleNames = exports.expandoButtonStyles = void 0;
var common_1 = __webpack_require__(0);
var settingsConfigKey = 'config';
exports.expandoButtonStyles = ['bigWide', 'big', 'medium', 'small'];
exports.expandoButtonStyleNames = {
    small: 'Small (icon)',
    medium: 'Medium',
    big: 'Big',
    bigWide: 'Big - extra wide'
};
exports.defaultExpandoButtonStyle = 'big';
exports.asExpandoButtonStyleOrDefault = function (x) {
    return R.includes(x, exports.expandoButtonStyles) ? x : exports.defaultExpandoButtonStyle;
};
exports.defaultConfig = Object.freeze({
    scriptVersion: common_1.scriptVersion,
    debug: {
        enabled: false,
        autoOpenSettings: false,
    },
    expandoButton: {
        enabled: true,
        resize: true,
        style: exports.defaultExpandoButtonStyle,
        alignRight: false,
        textClosed: '+',
        textOpened: '-',
        showComments: true,
    },
    infiniteScroll: {
        enabled: true,
        loadEarlier: false,
    },
    voting: {
        bigVoteArrowsOnMobile: true,
        clickEffect: true,
    },
    post: {
        openInNewTab: false,
        hidePostsFromGuilds: '',
        fullyHidePostsFromGuilds: false,
        blurNsfwThumbnails: false,
        hideAlreadyJoinedGuildsInDiscovery: true,
        improvedTableStyles: true,
        lessAggressivePatronBadges: false,
    },
    createPost: {
        loadTitleButton: true,
    },
    comment: {
        ctrlEnterToSend: true,
    },
    sidebar: {
        rightButton: true,
        autoHideRight: false,
    },
    external: {
        imgbbKey: '',
    }
});
var upgradeConfig = function (cfg) {
    return R.pipe(R.mergeDeepLeft(cfg), R.assoc('scriptVersion', common_1.scriptVersion))(exports.defaultConfig);
};
exports.readConfig = function (_a) {
    var forceUpgrade = (_a === void 0 ? {} : _a).forceUpgrade;
    return __awaiter(void 0, void 0, void 0, function () {
        var rawCfg, cfg, upgradedConfig;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0: return [4 /*yield*/, GM.getValue(settingsConfigKey, exports.defaultConfig)];
                case 1:
                    rawCfg = _b.sent();
                    if (!(rawCfg.scriptVersion !== common_1.scriptVersion || forceUpgrade)) return [3 /*break*/, 3];
                    console.log("[RuqES] Upgrading config " + rawCfg.scriptVersion + " -> " + common_1.scriptVersion);
                    upgradedConfig = upgradeConfig(rawCfg);
                    cfg = upgradedConfig;
                    return [4 /*yield*/, exports.writeConfig(upgradedConfig)];
                case 2:
                    _b.sent();
                    return [3 /*break*/, 4];
                case 3:
                    cfg = rawCfg;
                    _b.label = 4;
                case 4:
                    common_1.debugLog('readConfig', cfg);
                    if (cfg.debug) {
                        common_1.enabledDebug();
                    }
                    return [2 /*return*/, cfg];
            }
        });
    });
};
exports.writeConfig = function (x) { return __awaiter(void 0, void 0, void 0, function () { return __generator(this, function (_a) {
    switch (_a.label) {
        case 0: return [4 /*yield*/, GM.setValue(settingsConfigKey, x)];
        case 1: return [2 /*return*/, _a.sent()];
    }
}); }); }; // value can be an object, types are incorrect


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExpandoButtonModule = void 0;
var common_1 = __webpack_require__(0);
var styles_1 = __webpack_require__(1);
var selectors_1 = __webpack_require__(3);
var processors_1 = __webpack_require__(12);
var modules_1 = __webpack_require__(5);
var RuqESModule_1 = __webpack_require__(2);
var ExpandoButtonModule = /** @class */ (function (_super) {
    __extends(ExpandoButtonModule, _super);
    function ExpandoButtonModule() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.createExpandButton = function (cfg) { return $('<a>')
            .text(cfg.expandoButton.textClosed)
            .addClass(styles_1.expandBtnCls)
            .addClass(styles_1.expandBtnCls + "--style-" + cfg.expandoButton.style)
            .addClass('btn')
            .addClass('btn-secondary')
            .prop('role', 'button')
            .prop('href', 'javascript:void 0'); };
        _this.createBox = function () { return $('<div>')
            .addClass(styles_1.expandBoxCls)
            .addClass(styles_1.boxEmptyCls); };
        _this.genButtonClickHandler = function (el, link, postUrl, cfg) { return function (evt) {
            var box = el.find("." + styles_1.expandBoxCls);
            var boxOpened = box.hasClass(styles_1.expandBoxOpenedCls);
            var btn = $(evt.target);
            if (boxOpened) {
                btn.text(cfg.expandoButton.textClosed);
                box.removeClass(styles_1.expandBoxOpenedCls);
                return;
            }
            btn.text(cfg.expandoButton.textOpened);
            box.addClass(styles_1.expandBoxOpenedCls);
            if (box.hasClass(styles_1.boxEmptyCls)) {
                box.removeClass(styles_1.boxEmptyCls);
                processors_1.handleTextOfPost(postUrl, box, cfg);
                var processed = processors_1.linkHandlers.reduce(function (acc, x) { return acc || x(link, box, cfg); }, false);
                if (!processed) {
                    var unsupportedEl = $('<div>')
                        .append('Unsupported link type: ')
                        .append($('<code>').html($('<a>').prop('href', link).text(link)))
                        .append('.');
                    ExpandoButtonModule.insertWrappedElementIntoBox(box, unsupportedEl);
                }
            }
        }; };
        _this.processPostItem = function (cfg) { return function (_, domEl) {
            var el = $(domEl);
            var link = el.find('.card-header a').prop('href');
            var postUrl = el.find('.card-block .post-title a').prop('href');
            if (!link) {
                return;
            }
            el.append(_this.createBox());
            var btn = _this.createExpandButton(cfg);
            btn.click(_this.genButtonClickHandler(el, link, postUrl, cfg));
            var actions = el.find('.post-actions > ul');
            if (cfg.expandoButton.alignRight) {
                actions.append(btn);
            }
            else {
                actions.prepend(btn);
            }
        }; };
        return _this;
    }
    ExpandoButtonModule.prototype.setup = function (args, cfg) {
        var _a;
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_b) {
                if (!((_a = cfg === null || cfg === void 0 ? void 0 : cfg.expandoButton) === null || _a === void 0 ? void 0 : _a.enabled)) {
                    return [2 /*return*/];
                }
                if (!R.prop('silent', args || {})) {
                    common_1.debugLog('setupExpandoButton');
                }
                selectors_1.getPosts().filter(function (_, el) { return $(el).find("." + styles_1.expandBtnCls).length === 0; }).each(this.processPostItem(cfg));
                return [2 /*return*/];
            });
        });
    };
    ;
    ExpandoButtonModule.prototype.onContentChange = function (args, cfg) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.setup(args, cfg)];
            });
        });
    };
    ExpandoButtonModule.insertWrappedElementIntoBox = function (box, element, wrapperClass) {
        if (wrapperClass === void 0) { wrapperClass = styles_1.boxEmbedCls; }
        var wrapped = $('<div>').addClass(wrapperClass).html(element);
        ExpandoButtonModule.insertRawElementIntoBox(box, wrapped);
        modules_1.handleModulesAfterContentChange();
    };
    ExpandoButtonModule.insertRawElementIntoBox = function (box, element) {
        box.append(element);
    };
    ExpandoButtonModule.setupResizing = function (el) {
        var state = {
            resizing: false,
            startPoint: null,
            startValue: 100,
            value: 100,
        };
        el.mousedown(function (e) {
            state.resizing = true;
            state.startPoint = [e.clientX, e.clientY];
            state.startValue = state.value;
        });
        var doc = $(document);
        doc.mouseup(function (e) {
            state.resizing = false;
            state.startPoint = null;
        });
        doc.mousemove(function (e) {
            if (!state.resizing || !state.startPoint) {
                return;
            }
            var nx = e.clientX;
            var ny = e.clientY;
            var _a = __read(state.startPoint, 2), ox = _a[0], oy = _a[1];
            var dx = nx - ox;
            var dy = ny - oy;
            var dist = dx + dy;
            state.value = R.clamp(20, 500, state.startValue + dist / 5);
            var percVal = state.value + "%";
            el.css({ maxWidth: percVal, width: percVal });
        });
    };
    ExpandoButtonModule.insertImageIntoBox = function (box, link, cfg) {
        var _a;
        var el = $('<img>').prop('src', link).prop('draggable', false).addClass(styles_1.boxImgCls);
        if ((_a = cfg === null || cfg === void 0 ? void 0 : cfg.expandoButton) === null || _a === void 0 ? void 0 : _a.resize) {
            ExpandoButtonModule.setupResizing(el);
        }
        ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
    };
    return ExpandoButtonModule;
}(RuqESModule_1.RuqESModule));
exports.ExpandoButtonModule = ExpandoButtonModule;


/***/ }),
/* 8 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var styles_1 = __webpack_require__(1);
var common_1 = __webpack_require__(0);
var settings_1 = __webpack_require__(10);
var modules_1 = __webpack_require__(5);
var ExpandoButtonModule_1 = __webpack_require__(7);
var VotingModule_1 = __webpack_require__(13);
var SidebarModule_1 = __webpack_require__(14);
var CommentModule_1 = __webpack_require__(15);
var CreatePostModule_1 = __webpack_require__(16);
var InfiniteScrollModule_1 = __webpack_require__(17);
var PostsModule_1 = __webpack_require__(18);
var printInitMessage = function () {
    var bg = 'background-color: lightyellow;';
    var a = 'color: green; font-weight: bold; padding: 1em 0 1em 1em;' + bg;
    var b = 'color: black; padding: 1em 0; ' + bg;
    var c = 'color: purple; font-weight: bold; padding: 1em 1em 1em 0; ' + bg;
    var d = '';
    var info = GM.info;
    console.log("%c" + common_1.logPrefix + " " + common_1.scriptVersion + "%c created by %cenefi%c\n\n'" + common_1.scriptInfo.name + "' running via " + info.scriptHandler + " " + info.version + " on " + JSON.stringify(info.platform), a, b, c, d);
};
var init = function () {
    printInitMessage();
    [
        new InfiniteScrollModule_1.InfiniteScrollModule(),
        new ExpandoButtonModule_1.ExpandoButtonModule(),
        new VotingModule_1.VotingModule(),
        new PostsModule_1.PostsModule(),
        new CommentModule_1.CommentModule(),
        new SidebarModule_1.SidebarModule(),
        new CreatePostModule_1.CreatePostModule(),
    ].forEach(modules_1.registerClassModule);
};
var work = function () { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                styles_1.setupStyles(common_1.isDarkTheme());
                return [4 /*yield*/, settings_1.setupSettings()];
            case 1:
                _a.sent();
                return [4 /*yield*/, modules_1.handleFirstSetupOfModules()];
            case 2:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); };
init();
$(work);


/***/ }),
/* 9 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("/* -------------------------------------------------------------------\n  Microtip\n\n  Modern, lightweight css-only tooltips\n  Just 1kb minified and gzipped\n\n  @author Ghosh\n  @package Microtip\n\n----------------------------------------------------------------------\n  1. Base Styles\n  2. Direction Modifiers\n  3. Position Modifiers\n--------------------------------------------------------------------*/\n\n\n/* ------------------------------------------------\n  [1] Base Styles\n-------------------------------------------------*/\n\n[aria-label][role~=\"tooltip\"] {\n  position: relative;\n}\n\n[aria-label][role~=\"tooltip\"]::before,\n[aria-label][role~=\"tooltip\"]::after {\n  transform: translate3d(0, 0, 0);\n  -webkit-backface-visibility: hidden;\n  backface-visibility: hidden;\n  will-change: transform;\n  opacity: 0;\n  pointer-events: none;\n  transition: all var(--microtip-transition-duration, .18s) var(--microtip-transition-easing, ease-in-out) var(--microtip-transition-delay, 0s);\n  position: absolute;\n  box-sizing: border-box;\n  z-index: 10;\n  transform-origin: top;\n}\n\n[aria-label][role~=\"tooltip\"]::before {\n  background-size: 100% auto !important;\n  content: \"\";\n}\n\n[aria-label][role~=\"tooltip\"]::after {\n  background: rgba(17, 17, 17, .9);\n  border-radius: 4px;\n  color: #ffffff;\n  content: attr(aria-label);\n  font-size: var(--microtip-font-size, 13px);\n  font-weight: var(--microtip-font-weight, normal);\n  text-transform: var(--microtip-text-transform, none);\n  padding: .5em 1em;\n  white-space: nowrap;\n  box-sizing: content-box;\n}\n\n[aria-label][role~=\"tooltip\"]:hover::before,\n[aria-label][role~=\"tooltip\"]:hover::after,\n[aria-label][role~=\"tooltip\"]:focus::before,\n[aria-label][role~=\"tooltip\"]:focus::after {\n  opacity: 1;\n  pointer-events: auto;\n}\n\n\n\n/* ------------------------------------------------\n  [2] Position Modifiers\n-------------------------------------------------*/\n\n[role~=\"tooltip\"][data-microtip-position|=\"top\"]::before {\n  background: url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D%22http%3A//www.w3.org/2000/svg%22%20width%3D%2236px%22%20height%3D%2212px%22%3E%3Cpath%20fill%3D%22rgba%2817,%2017,%2017,%200.9%29%22%20transform%3D%22rotate%280%29%22%20d%3D%22M2.658,0.000%20C-13.615,0.000%2050.938,0.000%2034.662,0.000%20C28.662,0.000%2023.035,12.002%2018.660,12.002%20C14.285,12.002%208.594,0.000%202.658,0.000%20Z%22/%3E%3C/svg%3E\") no-repeat;\n  height: 6px;\n  width: 18px;\n  margin-bottom: 5px;\n}\n\n[role~=\"tooltip\"][data-microtip-position|=\"top\"]::after {\n  margin-bottom: 11px;\n}\n\n[role~=\"tooltip\"][data-microtip-position|=\"top\"]::before {\n  transform: translate3d(-50%, 0, 0);\n  bottom: 100%;\n  left: 50%;\n}\n\n[role~=\"tooltip\"][data-microtip-position|=\"top\"]:hover::before {\n  transform: translate3d(-50%, -5px, 0);\n}\n\n[role~=\"tooltip\"][data-microtip-position|=\"top\"]::after {\n  transform: translate3d(-50%, 0, 0);\n  bottom: 100%;\n  left: 50%;\n}\n\n[role~=\"tooltip\"][data-microtip-position=\"top\"]:hover::after {\n  transform: translate3d(-50%, -5px, 0);\n}\n\n/* ------------------------------------------------\n  [2.1] Top Left\n-------------------------------------------------*/\n[role~=\"tooltip\"][data-microtip-position=\"top-left\"]::after {\n  transform: translate3d(calc(-100% + 16px), 0, 0);\n  bottom: 100%;\n}\n\n[role~=\"tooltip\"][data-microtip-position=\"top-left\"]:hover::after {\n  transform: translate3d(calc(-100% + 16px), -5px, 0);\n}\n\n\n/* ------------------------------------------------\n  [2.2] Top Right\n-------------------------------------------------*/\n[role~=\"tooltip\"][data-microtip-position=\"top-right\"]::after {\n  transform: translate3d(calc(0% + -16px), 0, 0);\n  bottom: 100%;\n}\n\n[role~=\"tooltip\"][data-microtip-position=\"top-right\"]:hover::after {\n  transform: translate3d(calc(0% + -16px), -5px, 0);\n}\n\n\n/* ------------------------------------------------\n  [2.3] Bottom\n-------------------------------------------------*/\n[role~=\"tooltip\"][data-microtip-position|=\"bottom\"]::before {\n  background: url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D%22http%3A//www.w3.org/2000/svg%22%20width%3D%2236px%22%20height%3D%2212px%22%3E%3Cpath%20fill%3D%22rgba%2817,%2017,%2017,%200.9%29%22%20transform%3D%22rotate%28180%2018%206%29%22%20d%3D%22M2.658,0.000%20C-13.615,0.000%2050.938,0.000%2034.662,0.000%20C28.662,0.000%2023.035,12.002%2018.660,12.002%20C14.285,12.002%208.594,0.000%202.658,0.000%20Z%22/%3E%3C/svg%3E\") no-repeat;\n  height: 6px;\n  width: 18px;\n  margin-top: 5px;\n  margin-bottom: 0;\n}\n\n[role~=\"tooltip\"][data-microtip-position|=\"bottom\"]::after {\n  margin-top: 11px;\n}\n\n[role~=\"tooltip\"][data-microtip-position|=\"bottom\"]::before {\n  transform: translate3d(-50%, -10px, 0);\n  bottom: auto;\n  left: 50%;\n  top: 100%;\n}\n\n[role~=\"tooltip\"][data-microtip-position|=\"bottom\"]:hover::before {\n  transform: translate3d(-50%, 0, 0);\n}\n\n[role~=\"tooltip\"][data-microtip-position|=\"bottom\"]::after {\n  transform: translate3d(-50%, -10px, 0);\n  top: 100%;\n  left: 50%;\n}\n\n[role~=\"tooltip\"][data-microtip-position=\"bottom\"]:hover::after {\n  transform: translate3d(-50%, 0, 0);\n}\n\n\n/* ------------------------------------------------\n  [2.4] Bottom Left\n-------------------------------------------------*/\n[role~=\"tooltip\"][data-microtip-position=\"bottom-left\"]::after {\n  transform: translate3d(calc(-100% + 16px), -10px, 0);\n  top: 100%;\n}\n\n[role~=\"tooltip\"][data-microtip-position=\"bottom-left\"]:hover::after {\n  transform: translate3d(calc(-100% + 16px), 0, 0);\n}\n\n\n/* ------------------------------------------------\n  [2.5] Bottom Right\n-------------------------------------------------*/\n[role~=\"tooltip\"][data-microtip-position=\"bottom-right\"]::after {\n  transform: translate3d(calc(0% + -16px), -10px, 0);\n  top: 100%;\n}\n\n[role~=\"tooltip\"][data-microtip-position=\"bottom-right\"]:hover::after {\n  transform: translate3d(calc(0% + -16px), 0, 0);\n}\n\n\n/* ------------------------------------------------\n  [2.6] Left\n-------------------------------------------------*/\n[role~=\"tooltip\"][data-microtip-position=\"left\"]::before,\n[role~=\"tooltip\"][data-microtip-position=\"left\"]::after {\n  bottom: auto;\n  left: auto;\n  right: 100%;\n  top: 50%;\n  transform: translate3d(10px, -50%, 0);\n}\n\n[role~=\"tooltip\"][data-microtip-position=\"left\"]::before {\n  background: url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D%22http%3A//www.w3.org/2000/svg%22%20width%3D%2212px%22%20height%3D%2236px%22%3E%3Cpath%20fill%3D%22rgba%2817,%2017,%2017,%200.9%29%22%20transform%3D%22rotate%28-90%2018%2018%29%22%20d%3D%22M2.658,0.000%20C-13.615,0.000%2050.938,0.000%2034.662,0.000%20C28.662,0.000%2023.035,12.002%2018.660,12.002%20C14.285,12.002%208.594,0.000%202.658,0.000%20Z%22/%3E%3C/svg%3E\") no-repeat;\n  height: 18px;\n  width: 6px;\n  margin-right: 5px;\n  margin-bottom: 0;\n}\n\n[role~=\"tooltip\"][data-microtip-position=\"left\"]::after {\n  margin-right: 11px;\n}\n\n[role~=\"tooltip\"][data-microtip-position=\"left\"]:hover::before,\n[role~=\"tooltip\"][data-microtip-position=\"left\"]:hover::after {\n  transform: translate3d(0, -50%, 0);\n}\n\n\n/* ------------------------------------------------\n  [2.7] Right\n-------------------------------------------------*/\n[role~=\"tooltip\"][data-microtip-position=\"right\"]::before,\n[role~=\"tooltip\"][data-microtip-position=\"right\"]::after {\n  bottom: auto;\n  left: 100%;\n  top: 50%;\n  transform: translate3d(-10px, -50%, 0);\n}\n\n[role~=\"tooltip\"][data-microtip-position=\"right\"]::before {\n  background: url(\"data:image/svg+xml;charset=utf-8,%3Csvg%20xmlns%3D%22http%3A//www.w3.org/2000/svg%22%20width%3D%2212px%22%20height%3D%2236px%22%3E%3Cpath%20fill%3D%22rgba%2817,%2017,%2017,%200.9%29%22%20transform%3D%22rotate%2890%206%206%29%22%20d%3D%22M2.658,0.000%20C-13.615,0.000%2050.938,0.000%2034.662,0.000%20C28.662,0.000%2023.035,12.002%2018.660,12.002%20C14.285,12.002%208.594,0.000%202.658,0.000%20Z%22/%3E%3C/svg%3E\") no-repeat;\n  height: 18px;\n  width: 6px;\n  margin-bottom: 0;\n  margin-left: 5px;\n}\n\n[role~=\"tooltip\"][data-microtip-position=\"right\"]::after {\n  margin-left: 11px;\n}\n\n[role~=\"tooltip\"][data-microtip-position=\"right\"]:hover::before,\n[role~=\"tooltip\"][data-microtip-position=\"right\"]:hover::after {\n  transform: translate3d(0, -50%, 0);\n}\n\n/* ------------------------------------------------\n  [3] Size\n-------------------------------------------------*/\n[role~=\"tooltip\"][data-microtip-size=\"small\"]::after {\n  white-space: initial;\n  width: 80px;\n}\n\n[role~=\"tooltip\"][data-microtip-size=\"medium\"]::after {\n  white-space: initial;\n  width: 150px;\n}\n\n[role~=\"tooltip\"][data-microtip-size=\"large\"]::after {\n  white-space: initial;\n  width: 260px;\n}\n");

/***/ }),
/* 10 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.setupSettings = void 0;
var common_1 = __webpack_require__(0);
var styles_1 = __webpack_require__(1);
var config_1 = __webpack_require__(6);
var logo_svg_1 = __importDefault(__webpack_require__(4));
var downloader_1 = __webpack_require__(11);
var selectors_1 = __webpack_require__(3);
var genSettingsId = function (x) { return "" + styles_1.clsPrefix + x; };
var expandoButtonId = genSettingsId('expando');
var expandoButtonStyleId = genSettingsId('expando-style');
var expandoButtonAlignRightId = genSettingsId('expando-align-right');
var expandoButtonResizeId = genSettingsId('expando-resize');
var expandoButtonTextClosedId = genSettingsId('expando-text-closed');
var expandoButtonTextClosedListId = genSettingsId('expando-text-closed-list');
var expandoButtonTextOpenedId = genSettingsId('expando-text-opened');
var expandoButtonTextOpenedListId = genSettingsId('expando-text-opened-list');
var expandoButtonShowCommentsId = genSettingsId('expando-show-comments');
var infiniteScrollId = genSettingsId('infinite-scroll');
var infiniteScrollLoadEarlierId = genSettingsId('infinite-scroll-load-earlier');
var bigVoteArrowsOnMobileId = genSettingsId('big-vote-arrows-on-mobile');
var voteEffectId = genSettingsId('vote-effect');
var openPostInNewTabId = genSettingsId('post-open-in-new-tab');
var hidePostsFromGuildsId = genSettingsId('post-hide-posts-from-guilds');
var fullyHidePostsFromGuildsId = genSettingsId('post-fully-hide-posts-from-guilds');
var hideAlreadyJoinedGuildsInDiscoveryId = genSettingsId('post-hide-already-joined-guilds');
var blurNsfwThumbnailsId = genSettingsId('post-blur-nsfw-thumbnails');
var sidebarRightHideButtonId = genSettingsId('sidebar-right-hide-button');
var sidebarRightAutoHideId = genSettingsId('sidebar-right-auto-hide');
var createPostLoadTitleId = genSettingsId('create-post-load-title');
var operationsContainerId = genSettingsId('operations');
var importHelperId = genSettingsId('import-helper');
var imgBbKeyId = genSettingsId('img-bb-key');
var commentCtrlEnterToSendId = genSettingsId('comment-ctrl-enter-to-send');
var improvedTableStylesId = genSettingsId('improved-table-styles');
var lessAggressivePatronBadgesId = genSettingsId('less-aggressive-patron-badges');
var debugId = genSettingsId('debug');
var debugAutoOpenSettingsId = genSettingsId('debug-auto-open-settings');
var debugSectionId = genSettingsId('debug-section');
var debugForceUpgradeId = genSettingsId('debug-force-upgrade');
var setVisibilityOfDebugSectionInSettings = function (val) {
    return common_1.$i(debugSectionId).css({ display: val ? 'block' : 'none' });
};
var initializeSettingsForm = function (cfg) {
    var initChecked = function (id, val) { return common_1.$i(id).prop('checked', val); };
    var initVal = function (id, val) { return common_1.$i(id).val(val); };
    initChecked(expandoButtonId, cfg.expandoButton.enabled);
    initChecked(expandoButtonResizeId, cfg.expandoButton.resize);
    initVal(expandoButtonStyleId, cfg.expandoButton.style);
    initChecked(expandoButtonAlignRightId, cfg.expandoButton.alignRight);
    initVal(expandoButtonTextClosedId, cfg.expandoButton.textClosed);
    initVal(expandoButtonTextOpenedId, cfg.expandoButton.textOpened);
    initChecked(expandoButtonShowCommentsId, cfg.expandoButton.showComments);
    initChecked(infiniteScrollId, cfg.infiniteScroll.enabled);
    initChecked(infiniteScrollLoadEarlierId, cfg.infiniteScroll.loadEarlier);
    initChecked(bigVoteArrowsOnMobileId, cfg.voting.bigVoteArrowsOnMobile);
    initChecked(voteEffectId, cfg.voting.clickEffect);
    initChecked(openPostInNewTabId, cfg.post.openInNewTab);
    initVal(hidePostsFromGuildsId, cfg.post.hidePostsFromGuilds);
    initChecked(fullyHidePostsFromGuildsId, cfg.post.fullyHidePostsFromGuilds);
    initChecked(hideAlreadyJoinedGuildsInDiscoveryId, cfg.post.hideAlreadyJoinedGuildsInDiscovery);
    initChecked(blurNsfwThumbnailsId, cfg.post.blurNsfwThumbnails);
    initChecked(sidebarRightHideButtonId, cfg.sidebar.rightButton);
    initChecked(sidebarRightAutoHideId, cfg.sidebar.autoHideRight);
    initChecked(createPostLoadTitleId, cfg.createPost.loadTitleButton);
    initVal(imgBbKeyId, cfg.external.imgbbKey);
    initChecked(commentCtrlEnterToSendId, cfg.comment.ctrlEnterToSend);
    initChecked(improvedTableStylesId, cfg.post.improvedTableStyles);
    initChecked(lessAggressivePatronBadgesId, cfg.post.lessAggressivePatronBadges);
    var debugEnabled = cfg.debug.enabled;
    initChecked(debugId, debugEnabled);
    initChecked(debugAutoOpenSettingsId, cfg.debug.autoOpenSettings);
    setVisibilityOfDebugSectionInSettings(debugEnabled);
};
var openSettingsModal = function () { return __awaiter(void 0, void 0, void 0, function () {
    var _a;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                selectors_1.getTopLevelContainersForBlurring().addClass(styles_1.blurAnimatedCls).addClass(styles_1.blurCls);
                _a = initializeSettingsForm;
                return [4 /*yield*/, config_1.readConfig()];
            case 1:
                _a.apply(void 0, [_b.sent()]);
                common_1.$c(styles_1.settingsBackdropCls).show();
                common_1.$c(styles_1.settingsModalCls).addClass(styles_1.settingsModalVisibleCls);
                return [2 /*return*/];
        }
    });
}); };
var closeSettingsModal = function () {
    selectors_1.getTopLevelContainersForBlurring().removeClass(styles_1.blurCls);
    common_1.$c(styles_1.settingsModalCls).removeClass(styles_1.settingsModalVisibleCls);
    setTimeout(function () { return common_1.$c(styles_1.settingsBackdropCls).hide(); }, 200); // wait for transition to finish
};
var getConfigFromSettingsForm = function () {
    var getStrVal = function (id) { return common_1.$i(id).val(); };
    var getChecked = function (id) { return Boolean(common_1.$i(id).prop('checked')); };
    return ({
        expandoButton: {
            enabled: getChecked(expandoButtonId),
            resize: getChecked(expandoButtonResizeId),
            style: R.pipe(getStrVal, config_1.asExpandoButtonStyleOrDefault)(expandoButtonStyleId),
            alignRight: getChecked(expandoButtonAlignRightId),
            textClosed: getStrVal(expandoButtonTextClosedId),
            textOpened: getStrVal(expandoButtonTextOpenedId),
            showComments: getChecked(expandoButtonShowCommentsId),
        },
        infiniteScroll: {
            enabled: getChecked(infiniteScrollId),
            loadEarlier: getChecked(infiniteScrollLoadEarlierId),
        },
        voting: {
            bigVoteArrowsOnMobile: getChecked(bigVoteArrowsOnMobileId),
            clickEffect: getChecked(voteEffectId),
        },
        post: {
            openInNewTab: getChecked(openPostInNewTabId),
            hidePostsFromGuilds: getStrVal(hidePostsFromGuildsId),
            fullyHidePostsFromGuilds: getChecked(fullyHidePostsFromGuildsId),
            blurNsfwThumbnails: getChecked(blurNsfwThumbnailsId),
            hideAlreadyJoinedGuildsInDiscovery: getChecked(hideAlreadyJoinedGuildsInDiscoveryId),
            improvedTableStyles: getChecked(improvedTableStylesId),
            lessAggressivePatronBadges: getChecked(lessAggressivePatronBadgesId),
        },
        sidebar: {
            rightButton: getChecked(sidebarRightHideButtonId),
            autoHideRight: getChecked(sidebarRightAutoHideId),
        },
        createPost: {
            loadTitleButton: getChecked(createPostLoadTitleId),
        },
        comment: {
            ctrlEnterToSend: getChecked(commentCtrlEnterToSendId),
        },
        external: {
            imgbbKey: getStrVal(imgBbKeyId),
        },
        debug: {
            enabled: getChecked(debugId),
            autoOpenSettings: getChecked(debugAutoOpenSettingsId),
        },
    });
};
var saveAndCloseSettingsModal = function () { return __awaiter(void 0, void 0, void 0, function () {
    var newValues, _a;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                _a = [{}];
                return [4 /*yield*/, config_1.readConfig()];
            case 1:
                newValues = __assign.apply(void 0, [__assign.apply(void 0, _a.concat([_b.sent()])), getConfigFromSettingsForm()]);
                common_1.debugLog('generated new config', newValues);
                return [4 /*yield*/, config_1.writeConfig(newValues)];
            case 2:
                _b.sent();
                closeSettingsModal();
                return [2 /*return*/];
        }
    });
}); };
var saveAndReloadSettings = function () { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, saveAndCloseSettingsModal()];
            case 1:
                _a.sent();
                window.location.reload();
                return [2 /*return*/];
        }
    });
}); };
var resetSettings = function () { return __awaiter(void 0, void 0, void 0, function () {
    var _a;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0: return [4 /*yield*/, config_1.writeConfig(config_1.defaultConfig)];
            case 1:
                _b.sent();
                _a = initializeSettingsForm;
                return [4 /*yield*/, config_1.readConfig()];
            case 2:
                _a.apply(void 0, [_b.sent()]);
                return [2 /*return*/];
        }
    });
}); };
var exportSettings = function () { return __awaiter(void 0, void 0, void 0, function () {
    var cfg;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, config_1.readConfig()];
            case 1:
                cfg = _a.sent();
                downloader_1.download(JSON.stringify(cfg), 'application/json', 'RuqES-settings.json');
                return [2 /*return*/];
        }
    });
}); };
var importSettingsFileConfirmed = function (evt) {
    common_1.debugLog('importSettingsFileConfirmed', evt);
    var files = common_1.$i(importHelperId).prop('files');
    if (!files || !files[0]) {
        return;
    }
    var file = files[0];
    common_1.debugLog('got file', file);
    var reader = new FileReader();
    reader.onload = function (readEvt) { return __awaiter(void 0, void 0, void 0, function () {
        var text, newCfg;
        var _a;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    text = (_a = readEvt === null || readEvt === void 0 ? void 0 : readEvt.target) === null || _a === void 0 ? void 0 : _a.result;
                    common_1.debugLog('read result', text);
                    newCfg = null;
                    try {
                        newCfg = JSON.parse(text);
                    } // 
                    catch (e) {
                        alert("Failed to parse given file as JSON.");
                    }
                    if (!(newCfg !== null)) return [3 /*break*/, 2];
                    return [4 /*yield*/, config_1.writeConfig(newCfg)];
                case 1:
                    _b.sent();
                    window.location.reload();
                    _b.label = 2;
                case 2: return [2 /*return*/];
            }
        });
    }); };
    reader.readAsBinaryString(file);
};
var importSettings = function () { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        common_1.$i(importHelperId).click();
        return [2 /*return*/];
    });
}); };
var createSettingsDialog = function () {
    var withTooltip = function (text, html) { return function (position) {
        if (position === void 0) { position = "bottom"; }
        return "<span role=\"tooltip\" aria-label=\"" + text + "\" data-microtip-position=\"" + position + "\">" + html + "</span>";
    }; };
    var mobileBadge = "<span class=\"badge badge-secondary\">mobile</span>";
    var desktopBadge = "<span class=\"badge badge-dark\">desktop</span>";
    var experimentalBadge = "<span class=\"badge badge-danger\">experimental</span>";
    var infiniteScrollCoolDownBadge = withTooltip('Adds cool-down to infinite scroll', "<span class=\"badge badge-warning\">IS CD</span>");
    var expandoButtonStyleOptions = config_1.expandoButtonStyles.map(function (x) { return "<option value=\"" + x + "\">" + config_1.expandoButtonStyleNames[x] + "</option>"; }).join('\n');
    var content = "\n<hr class=\"mt-0 mb-2\">\n<form class=\"" + styles_1.formCls + "\">\n<div class=\"form-group mb-2\">\n    <input type=\"checkbox\" id=\"" + expandoButtonId + "\" class=\"form-control\" />\n    <label for=\"" + expandoButtonId + "\">Expando button</label>\n</div>\n<div class=\"form-group mb-2 ml-3\">\n    <label for=\"" + expandoButtonStyleId + "\" class=\"mr-1\">Style</label>\n    <select id=\"" + expandoButtonStyleId + "\" class=\"form-control\">\n        " + expandoButtonStyleOptions + "\n    </select>\n</div>\n<div class=\"form-group mb-2 ml-3\">\n    <input type=\"checkbox\" id=\"" + expandoButtonShowCommentsId + "\" class=\"form-control\" />\n    <label for=\"" + expandoButtonShowCommentsId + "\" class=\"mr-1\">Show comments</label>" + experimentalBadge + "\n</div>\n<div class=\"form-group mb-2 ml-3\">\n    <input type=\"checkbox\" id=\"" + expandoButtonResizeId + "\" class=\"form-control\" />\n    <label for=\"" + expandoButtonResizeId + "\">Image resizing " + desktopBadge + "</label>\n</div>\n<div class=\"form-group mb-2 ml-3\">\n    <input type=\"checkbox\" id=\"" + expandoButtonAlignRightId + "\" class=\"form-control\" />\n    <label for=\"" + expandoButtonAlignRightId + "\">Align right " + mobileBadge + "</label>\n</div>\n<div class=\"form-group mb-2 ml-3 " + styles_1.settingsExpandoButtonActionCharacterCls + "\">\n    <label for=\"" + expandoButtonTextClosedId + "\" class=\"mr-1\">Open character:</label><br />\n    <input type=\"text\" id=\"" + expandoButtonTextClosedId + "\" class=\"form-control " + styles_1.widthAutoCls + "\" maxlength=\"5\" list=\"" + expandoButtonTextClosedListId + "\" />\n    <datalist id=\"" + expandoButtonTextClosedListId + "\">\n        <option value=\"+\">\n        <option value=\"\u2629\">\n        <option value=\"\u2795\">\n        <option value=\"\u1690\">\n        <option value=\"\u29FE\">\n        <option value=\"\u2295\">\n        <option value=\"\u2261\">\n        <option value=\"\u2261\">\n        <option value=\"\uD834\uDF62\">\n        <option value=\"\u2263\">\n        <option value=\"\uD834\uDF63\">\n        <option value=\"\uD834\uDF09\">\n    </datalist>\n</div>\n<div class=\"form-group mb-2 ml-3 " + styles_1.settingsExpandoButtonActionCharacterCls + "\">\n    <label for=\"" + expandoButtonTextOpenedId + "\" class=\"mr-1\">Close character:</label><br />\n    <input type=\"text\" id=\"" + expandoButtonTextOpenedId + "\" class=\"form-control " + styles_1.widthAutoCls + "\" maxlength=\"5\" list=\"" + expandoButtonTextOpenedListId + "\" />\n        <datalist id=\"" + expandoButtonTextOpenedListId + "\">\n        <option value=\"-\">\n        <option value=\"\u2013\">\n        <option value=\"\u2015\">\n        <option value=\"\u29FF\">\n        <option value=\"\u29FF\">\n        <option value=\"\u2796\">\n        <option value=\"\u229D\">\n        <option value=\"\u0398\">\n    </datalist>\n</div>\n\n<div class=\"form-group mb-2\">\n    <input type=\"checkbox\" id=\"" + infiniteScrollId + "\" class=\"form-control\" />\n    <label for=\"" + infiniteScrollId + "\">Infinite scroll</label>\n</div>\n<div class=\"form-group mb-2 ml-3\">\n    <input type=\"checkbox\" id=\"" + infiniteScrollLoadEarlierId + "\" class=\"form-control\" />\n    <label for=\"" + infiniteScrollLoadEarlierId + "\" class=\"mr-1\">Start loading next page earlier</label>" + infiniteScrollCoolDownBadge('left') + "\n</div>\n\n<div class=\"form-group mb-2\">\n    <input type=\"checkbox\" id=\"" + bigVoteArrowsOnMobileId + "\" class=\"form-control\" />\n    <label for=\"" + bigVoteArrowsOnMobileId + "\">Bigger vote arrows " + mobileBadge + "</label>\n</div>\n\n<div class=\"form-group mb-2\">\n    <input type=\"checkbox\" id=\"" + voteEffectId + "\" class=\"form-control\" />\n    <label for=\"" + voteEffectId + "\">Vote effect " + mobileBadge + "</label>\n</div>\n\n<div class=\"form-group mb-2\">\n    <input type=\"checkbox\" id=\"" + improvedTableStylesId + "\" class=\"form-control\" />\n    <label for=\"" + improvedTableStylesId + "\">Improved styles for tables in posts</label>\n</div>\n\n<div class=\"form-group mb-2\">\n    <input type=\"checkbox\" id=\"" + openPostInNewTabId + "\" class=\"form-control\" />\n    <label for=\"" + openPostInNewTabId + "\">Open posts in a new tab</label>\n</div>\n\n<div class=\"form-group mb-2 d-block\">\n    <label for=\"" + hidePostsFromGuildsId + "\">Semi-hide posts from guilds:</label><br />\n    <input type=\"text\" id=\"" + hidePostsFromGuildsId + "\" class=\"form-control\" placeholder=\"Guild names without '+' separated by comma ','\" />\n</div>\n<div class=\"form-group mb-2 ml-3\">\n    <input type=\"checkbox\" id=\"" + fullyHidePostsFromGuildsId + "\" class=\"form-control\" />\n    <label for=\"" + fullyHidePostsFromGuildsId + "\" class=\"mr-1\">Fully hide</label> " + infiniteScrollCoolDownBadge() + "\n</div>\n\n<div class=\"form-group mb-2\">\n    <input type=\"checkbox\" id=\"" + hideAlreadyJoinedGuildsInDiscoveryId + "\" class=\"form-control\" />\n    <label for=\"" + hideAlreadyJoinedGuildsInDiscoveryId + "\" class=\"mr-1\">Hide joined guilds on \"discover\" page</label> " + infiniteScrollCoolDownBadge('left') + "\n</div>\n\n<div class=\"form-group mb-2\">\n    <input type=\"checkbox\" id=\"" + blurNsfwThumbnailsId + "\" class=\"form-control\" />\n    <label for=\"" + blurNsfwThumbnailsId + "\">Blur thumbnails of NSFW posts</label>\n</div>\n\n<div class=\"form-group mb-2\">\n    <input type=\"checkbox\" id=\"" + sidebarRightHideButtonId + "\" class=\"form-control\" />\n    <label for=\"" + sidebarRightHideButtonId + "\">Enable hide button for right sidebar</label>\n</div>\n<div class=\"form-group mb-2 ml-3\">\n    <input type=\"checkbox\" id=\"" + sidebarRightAutoHideId + "\" class=\"form-control\" />\n    <label for=\"" + sidebarRightAutoHideId + "\">Auto-hide right sidebar</label>\n</div>\n\n<div class=\"form-group mb-2\">\n    <input type=\"checkbox\" id=\"" + createPostLoadTitleId + "\" class=\"form-control\" />\n    <label for=\"" + createPostLoadTitleId + "\">\"Load title\" button on create post page</label>\n</div>\n\n<div class=\"form-group mb-2 d-block\">\n    <label for=\"" + imgBbKeyId + "\">\n        <a href=\"https://api.imgbb.com/\" target=\"_blank\">imgbb</a> API key:\n    </label><br />\n    <input type=\"text\" id=\"" + imgBbKeyId + "\" class=\"form-control\" />\n</div>\n\n<div class=\"form-group mb-2\">\n    <input type=\"checkbox\" id=\"" + commentCtrlEnterToSendId + "\" class=\"form-control\" />\n    <label for=\"" + commentCtrlEnterToSendId + "\"><kbd>CTRL</kbd> + <kbd>ENTER</kbd> to send comment</label>\n</div>\n\n<div class=\"form-group mb-2\">\n    <input type=\"checkbox\" id=\"" + lessAggressivePatronBadgesId + "\" class=\"form-control\" />\n    <label for=\"" + lessAggressivePatronBadgesId + "\">Less aggressive patron badges</label>\n</div>\n\n<hr class=\"my-1\">\n\n<div class=\"form-group mb-1\" style=\"opacity: 0.5\">\n    <input type=\"checkbox\" id=\"" + debugId + "\" class=\"form-control\" />\n    <label for=\"" + debugId + "\">Debug mode</label>\n</div>\n<div id=\"" + debugSectionId + "\" class=\"ml-3\">\n    <div class=\"form-group mb-2\">\n        <input type=\"checkbox\" id=\"" + debugAutoOpenSettingsId + "\" class=\"form-control\" />\n        <label for=\"" + debugAutoOpenSettingsId + "\">Auto-open settings</label>\n    </div>\n    <a class=\"btn btn-primary " + styles_1.btnPrimaryCls + " mb-2\" id=\"" + debugForceUpgradeId + "\">Force config upgrade</a>\n</div>\n\n<hr class=\"my-1\">\n\n<div class=\"mb-1\"><strong>Settings operations</strong></div>\n<div id=\"" + operationsContainerId + "\" class=\"d-flex\">\n    <input id=\"" + importHelperId + "\" type=\"file\" class=\"d-none\" accept=\"application/json\">\n</div>\n\n<hr class=\"my-2\">\n\n<div>Changes will take effect after a page reload.</div>\n\n<hr class=\"my-2\">\n\n</form>\n    ";
    var captionPart = "\n<div class=\"d-flex justify-content-center align-items-baseline\">\n<div class=\"" + styles_1.settingsLogoCls + "\">" + logo_svg_1.default + "</div>\n<h1 class=\"" + styles_1.settingsCaptionCls + "\">RuqES</h1>\n<div class=\"ml-1\" style=\"opacity: 0.5\">\n  <span class=\"position-absolute\" style=\"top: 0.8em\">" + common_1.scriptVersion + "</span>\n  by <a href=\"https://ruqqus.com/@enefi\">@enefi</a>\n</div>\n</div>\n    ";
    var contentEl = $("<div>" + content + "</div>");
    contentEl.find('form').submit(function () {
        saveAndReloadSettings();
        return false;
    });
    var cancelButton = $('<a class="btn btn-secondary">Cancel</a>').click(closeSettingsModal);
    var saveButton = $('<a class="btn btn-secondary">Save</a>').click(saveAndCloseSettingsModal);
    var saveAndReloadButton = $('<a class="btn btn-primary ml-2">Save & reload</a>').addClass(styles_1.btnPrimaryCls).click(saveAndReloadSettings);
    var exportButton = $('<a class="btn btn-secondary">Export</a>').click(exportSettings);
    var importButton = $('<a class="btn btn-secondary ml-2">Import</a>').click(importSettings);
    var resetButton = $('<a class="btn btn-danger ml-2">Reset</a>').addClass(styles_1.btnDangerCls).click(resetSettings);
    contentEl.find("#" + operationsContainerId)
        .append(exportButton)
        .append(importButton)
        .append(resetButton);
    contentEl.find("#" + importHelperId).change(importSettingsFileConfirmed);
    var modal = $('<div>')
        .addClass(styles_1.settingsModalCls)
        .append(captionPart)
        .append(contentEl)
        .append($('<div>').addClass(['d-flex', 'justify-content-between', 'mt-2'])
        .append(cancelButton)
        .append($('<div>')
        .append(saveButton)
        .append(saveAndReloadButton)));
    common_1.$i(debugForceUpgradeId, modal).click(function () { return config_1.readConfig({ forceUpgrade: true }); });
    common_1.$i(debugId, modal).change(function (x) { return setVisibilityOfDebugSectionInSettings(x.target.checked); });
    var backdrop = $('<div>')
        .addClass(styles_1.settingsBackdropCls)
        .click(function (evt) {
        if ($(evt.target).hasClass(styles_1.settingsBackdropCls)) {
            closeSettingsModal();
        }
    });
    return backdrop.append(modal).hide();
};
var setupCreatorMark = function () {
    var logoEl = $('<span>').html(logo_svg_1.default).addClass(styles_1.creatorMarkCls).prop('title', 'RuqES developer');
    common_1.$c('user-name').filter(function (_, el) { return $(el).text() === 'enefi'; }).addClass(styles_1.creatorNameCls).after(logoEl);
};
var createSettingsButtonForDesktop = function () {
    var icon = $('<i class="fas fa-wrench fa-width-rem text-left mr-3"></i>');
    return $('<a>')
        .prop('class', 'dropdown-item')
        .text('RuqES')
        .prepend(icon)
        .prop('href', 'javascript:void(0)')
        .click(openSettingsModal);
};
var createSettingsButtonForMobile = function () {
    var icon = $('<i class="fas fa-wrench fa-width-rem mr-3">');
    var link = $('<a>')
        .addClass('nav-link')
        .append(icon)
        .append('RuqES')
        .click(openSettingsModal)
        .click(function () { return $('.navbar-toggler').click(); });
    return $('<li class="nav-item">').append(link);
};
exports.setupSettings = function () { return __awaiter(void 0, void 0, void 0, function () {
    var cfg;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, config_1.readConfig()];
            case 1:
                cfg = _a.sent();
                common_1.debugLog('setupSettings', cfg);
                setupCreatorMark();
                $('#navbar #navbarResponsive > ul > li:last-child a[href="/settings"]').after(createSettingsButtonForDesktop());
                $('#navbarResponsive > ul:last-child a.nav-link[href="/settings"]').parent().after(createSettingsButtonForMobile());
                $('body').prepend(createSettingsDialog());
                if (!cfg.debug.autoOpenSettings) return [3 /*break*/, 3];
                return [4 /*yield*/, openSettingsModal()];
            case 2:
                _a.sent();
                _a.label = 3;
            case 3: return [2 /*return*/];
        }
    });
}); };


/***/ }),
/* 11 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

Object.defineProperty(exports, "__esModule", { value: true });
exports.download = void 0;
var downloadURI = function (uri, name) {
    var link = document.createElement("a");
    link.download = name;
    link.href = uri;
    link.click();
};
exports.download = function (data, type, name) {
    var blob = new Blob([data], { type: type });
    var url = window.URL.createObjectURL(blob);
    downloadURI(url, name);
    window.URL.revokeObjectURL(url);
};


/***/ }),
/* 12 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.linkHandlers = exports.handleTextOfPost = void 0;
var common_1 = __webpack_require__(0);
var styles_1 = __webpack_require__(1);
var ExpandoButtonModule_1 = __webpack_require__(7);
var isDirectImageLink = function (x) { return /\.(?:jpg|jpeg|png|gif|svg|webp)(?:\?.*)?$|^https:\/\/i.ruqqus.com\//i.test(x); };
var handleDirectImageLink = function (link, box, cfg) {
    if (!isDirectImageLink(link)) {
        return false;
    }
    ExpandoButtonModule_1.ExpandoButtonModule.insertImageIntoBox(box, link, cfg);
    return true;
};
var imgurGifvLinkRegexGen = function () { return /https:\/\/i\.imgur\.com\/(\w+)\.(gifv|mp4)/g; };
var isImgurGifvLink = function (x) { return imgurGifvLinkRegexGen().test(x); };
var handleImgurGifv = function (link, box) {
    if (!isImgurGifvLink(link)) {
        return false;
    }
    var matches = __spread(link.matchAll(imgurGifvLinkRegexGen()));
    var id = R.path([0, 1], matches);
    var el = $("<div class=\"handleImgurGifv\"></div><blockquote class=\"imgur-embed-pub\" lang=\"en\" data-id=\"" + id + "\"><a href=\"//imgur.com/" + id + "\">?</a></blockquote><script async src=\"//s.imgur.com/min/embed.js\" charset=\"utf-8\"></script></div>");
    ExpandoButtonModule_1.ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
    return true;
};
var brokenImgurRegexGen = function () { return /^https:\/\/(?:m\.)?imgur\.com\/.*?([a-zA-Z0-9]+)$/gm; };
var isBrokenImgurLink = function (x) { return brokenImgurRegexGen().test(x) && !x.includes('gallery'); };
var handleBrokenImgur = function (link, box, cfg) {
    if (!isBrokenImgurLink(link)) {
        return false;
    }
    common_1.xhr({
        method: 'GET',
        url: link,
        onload: function (resp) {
            var parsed = $(resp.response);
            var imgContainer = parsed.find('.post-image-container');
            var imgId = imgContainer.prop('id');
            var src = "https://i.imgur.com/" + imgId + ".png";
            ExpandoButtonModule_1.ExpandoButtonModule.insertImageIntoBox(box, src, cfg);
        },
    });
    return true;
};
var writeErrorToExpandButtonBox = function (box, errorText, retryCb) {
    if (retryCb === void 0) { retryCb = null; }
    var errorEl = $('<div>')
        .addClass(styles_1.errorTextCls)
        .append($('<span>').text(errorText));
    if (retryCb) {
        var retryButton = $('<button>')
            .text('Retry')
            .addClass('btn')
            .addClass('btn-primary')
            .addClass('ml-1')
            .click(function () {
            box.find("." + styles_1.errorTextCls).remove();
            retryCb();
        });
        errorEl.append(retryButton);
    }
    box.prepend(errorEl);
};
var onFetchingPostFailed = function (postUrl, box, cfg) { return function (resp) {
    console.error('Failed to fetch a post.', resp);
    box.find("." + styles_1.postLoadingCls).remove();
    writeErrorToExpandButtonBox(box, "Failed to fetch the post. Error " + resp.status + ": " + resp.statusText, function () { return exports.handleTextOfPost(postUrl, box, cfg); });
}; };
exports.handleTextOfPost = function (postUrl, box, cfg) {
    box.append(common_1.createTextLoader('Fetching post', styles_1.postLoadingCls));
    common_1.xhr({
        method: 'GET',
        url: postUrl,
        onload: function (resp) {
            if (resp.status !== 200) {
                return onFetchingPostFailed(postUrl, box, cfg)(resp);
            }
            var parsed = $(resp.response);
            var post = parsed.find('#post-body').children().filter(':not(.embed-responsive)');
            box.find("." + styles_1.postLoadingCls).remove();
            var emptyPost = !post.length || post.text().trim() === '';
            var comments = cfg.expandoButton.showComments ? parsed.find('.comment-section:not(.text-center) > .comment') : $();
            var emptyComments = comments.length === 0;
            if (emptyPost && emptyComments) {
                return;
            }
            var wrappedComments = $('<div>').addClass(styles_1.boxPostCommentsCls).append(comments).hide();
            var showCommentsButton = $('<a>').addClass('btn btn-secondary').text('Toggle comments').click(function (evt) {
                var el = $(evt.target);
                el.parent().find("." + styles_1.boxPostCommentsCls).toggle();
            });
            var combined = $('<div>').append(post);
            if (!emptyComments) {
                combined.append(showCommentsButton).append(wrappedComments);
            }
            ExpandoButtonModule_1.ExpandoButtonModule.insertWrappedElementIntoBox(box, combined, styles_1.boxPostTextCls);
        },
        onerror: onFetchingPostFailed(postUrl, box, cfg),
    });
};
var isRuqqusPost = function (x) { return /^https:\/\/ruqqus\.com\/post\/.*$/.test(x); };
var handleRuqqusPost = function (link, box) {
    if (!isRuqqusPost(link)) {
        return false;
    }
    // already handled by handleTextOfPost
    return true;
};
var imgurAlbumPrefix = 'https://imgur.com/a/';
var isImgurAlbum = function (x) { return x.startsWith(imgurAlbumPrefix); };
var handleImgurAlbum = function (link, box) {
    if (!isImgurAlbum(link)) {
        return false;
    }
    var albumId = R.drop(imgurAlbumPrefix.length, link);
    var el = $("<div class=\"handleImgurAlbum embed-responsive\"></div><blockquote class=\"imgur-embed-pub\" lang=\"en\" data-id=\"a/" + albumId + "\"><a href=\"//imgur.com/a/" + albumId + "\">?</a></blockquote><script async src=\"//s.imgur.com/min/embed.js\" charset=\"utf-8\"></script></div>");
    ExpandoButtonModule_1.ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
    return true;
};
var genYouTubeVideoRegex = function () { return /^https:\/\/(?:www|m)\.youtube\.com\/watch\?v=([\w-]+).*$|^https:\/\/youtu\.be\/([\w-]+)$/g; };
var isYouTubeVideo = function (x) { return genYouTubeVideoRegex().test(x); };
var handleYouTubeVideo = function (link, box) {
    if (!isYouTubeVideo(link)) {
        return false;
    }
    var matches = __spread(link.matchAll(genYouTubeVideoRegex()));
    var id = R.path([0, 1], matches) || R.path([0, 2], matches);
    if (!id) {
        console.log('failed to get YouTube video id', link);
        return false;
    }
    var el = $("<div class=\"handleYouTubeVideo embed-responsive embed-responsive-16by9\"><iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/" + id + "\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe></div>");
    ExpandoButtonModule_1.ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
    return true;
};
var genBitChuteVideoRegex = function () { return /^https:\/\/www\.bitchute\.com\/video\/(\w+).*$/g; };
var isBitChuteVideo = function (x) { return genBitChuteVideoRegex().test(x); };
var handleBitChuteVideo = function (link, box) {
    if (!isBitChuteVideo(link)) {
        return false;
    }
    var matches = __spread(link.matchAll(genBitChuteVideoRegex()));
    var id = R.path([0, 1], matches);
    if (!id) {
        console.log('failed to get BitChute video id', link);
        return false;
    }
    var el = $("<div class=\"handleBitChuteVideo embed-responsive embed-responsive-16by9\"><iframe width=\"640\" height=\"360\" scrolling=\"no\" frameborder=\"0\" style=\"border: none;\" src=\"https://www.bitchute.com/embed/" + id + "/\"></iframe></div>");
    ExpandoButtonModule_1.ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
    return true;
};
var genRedGifsVideoRegex = function () { return /^https:\/\/redgifs\.com\/watch\/([\w_\d]+).*$/g; };
var isRedGifsVideo = function (x) { return genRedGifsVideoRegex().test(x); };
var handleRedGifsVideo = function (link, box) {
    if (!isRedGifsVideo(link)) {
        return false;
    }
    var matches = __spread(link.matchAll(genRedGifsVideoRegex()));
    var id = R.path([0, 1], matches);
    if (!id) {
        console.log('failed to get RedGifs video id', link);
        return false;
    }
    var el = $("<div class=\"handleRedGifsVideo embed-responsive embed-responsive-16by9\"><iframe src='https://redgifs.com/ifr/" + id + "' frameborder='0' scrolling='no' allowfullscreen width='640' height='360'></iframe></div>");
    ExpandoButtonModule_1.ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
    return true;
};
var genGfycatVideoRegex = function () { return /^https:\/\/gfycat\.com\/([\w_\d]+).*$/g; };
var isGfycatVideo = function (x) { return genGfycatVideoRegex().test(x); };
var handleGfycatVideo = function (link, box) {
    if (!isGfycatVideo(link)) {
        return false;
    }
    var matches = __spread(link.matchAll(genGfycatVideoRegex()));
    var id = R.path([0, 1], matches);
    if (!id) {
        console.log('failed to get Gfycat video id', link);
        return false;
    }
    var el = $("<div class=\"handleGfycatVideo embed-responsive embed-responsive-16by9\"><iframe src='https://gfycat.com/ifr/" + id + "' frameborder='0' scrolling='no' allowfullscreen width='640' height='492'></iframe></div>");
    ExpandoButtonModule_1.ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
    return true;
};
var gifDeliveryNetworkRegexGen = function () { return /^https:\/\/(?:www\.)gifdeliverynetwork\.com\/([\w_\d]+).*$/gm; };
var isGifDeliveryNetworkLink = function (x) { return gifDeliveryNetworkRegexGen().test(x); };
var handleGifDeliveryNetwork = function (link, box) {
    if (!isGifDeliveryNetworkLink(link)) {
        return false;
    }
    var matches = __spread(link.matchAll(gifDeliveryNetworkRegexGen()));
    var id = R.path([0, 1], matches);
    if (!id) {
        console.log('failed to get GifDeliveryNetwork video id', link);
        return false;
    }
    var el = $("<div class=\"handleGfycatVideo embed-responsive embed-responsive-16by9\"><iframe src='https://gfycat.com/ifr/" + id + "' frameborder='0' scrolling='no' allowfullscreen width='640' height='492'></iframe></div>");
    ExpandoButtonModule_1.ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
    return true;
};
var imgflipLinkRegex = function () { return /^https:\/\/imgflip\.com\/i\/.*?([a-zA-Z0-9]+)$/gm; };
var isImgflipLink = function (x) { return imgflipLinkRegex().test(x); };
var handleImgflipLink = function (link, box, cfg) {
    if (!isImgflipLink(link)) {
        return false;
    }
    var matches = __spread(link.matchAll(imgflipLinkRegex()));
    var id = R.path([0, 1], matches);
    var src = "https://i.imgflip.com/" + id + ".jpg";
    ExpandoButtonModule_1.ExpandoButtonModule.insertImageIntoBox(box, src, cfg);
    return true;
};
var ibbcoLinkRegex = function () { return /^https:\/\/ibb.co\//gm; };
var isIbbcoLink = function (x) { return ibbcoLinkRegex().test(x); };
var handleIbbcoLink = function (link, box, cfg) {
    if (!isIbbcoLink(link)) {
        return false;
    }
    common_1.xhr({
        method: 'GET',
        url: link,
        onload: function (resp) {
            var parsed = $(resp.response);
            var img = parsed.find('#image-viewer-container img');
            var imgUrl = img.prop('src');
            ExpandoButtonModule_1.ExpandoButtonModule.insertImageIntoBox(box, imgUrl, cfg);
        },
    });
    return true;
};
var twitterLinkRegex = function () { return /^https:\/\/(?:mobile\.)?twitter\.com\/([\w0-9]+)\/status\/(\d+)$/gm; };
var isTwitterLink = function (x) { return twitterLinkRegex().test(x); };
var handleTwitterLink = function (link, box) {
    if (!isTwitterLink(link)) {
        return false;
    }
    common_1.xhr({
        method: 'GET',
        url: "https://publish.twitter.com/oembed?url=" + encodeURIComponent(link),
        onload: function (resp) {
            var data = JSON.parse(resp.response);
            common_1.debugLog('handleTwitterLink', data);
            var parsed = $(data.html);
            // TODO: option to remove script
            var el = $('<div>').css({ margin: '-10px 0' }).html(parsed);
            ExpandoButtonModule_1.ExpandoButtonModule.insertWrappedElementIntoBox(box, el);
        },
    });
    return true;
};
exports.linkHandlers = [
    handleDirectImageLink,
    handleRuqqusPost,
    handleImgurAlbum,
    handleBrokenImgur,
    handleYouTubeVideo,
    handleBitChuteVideo,
    handleRedGifsVideo,
    handleGfycatVideo,
    handleGifDeliveryNetwork,
    handleImgurGifv,
    handleImgflipLink,
    handleIbbcoLink,
    handleTwitterLink,
];


/***/ }),
/* 13 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.VotingModule = void 0;
var styles_1 = __webpack_require__(1);
var common_1 = __webpack_require__(0);
var RuqESModule_1 = __webpack_require__(2);
var VotingModule = /** @class */ (function (_super) {
    __extends(VotingModule, _super);
    function VotingModule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    VotingModule.prototype.setup = function (args, cfg) {
        return __awaiter(this, void 0, void 0, function () {
            var votingCfg;
            return __generator(this, function (_a) {
                votingCfg = cfg.voting || {};
                if (!R.prop('silent', args || {})) {
                    common_1.debugLog('setupVoting', votingCfg);
                }
                if (votingCfg.bigVoteArrowsOnMobile) {
                    $('.voting.d-md-none').css('margin-bottom', '-1rem').css('margin-top', '-0.4rem')
                        .find('span.arrow-mobile-up, span.arrow-mobile-down').css('height', '3rem').css('font-size', '1.5rem')
                        .find('i').css('font-size', '1.5rem');
                }
                if (votingCfg.clickEffect) {
                    $('span.arrow-mobile-up, span.arrow-mobile-down').click(function (evt) {
                        var evtTargetEl = $(evt.target);
                        var el = evtTargetEl;
                        if (el.prop('tagName').toLowerCase() === 'i') {
                            el = el.parent();
                        }
                        if (el.prop('tagName').toLowerCase() !== 'span') {
                            common_1.printError('clickEffect: failed to locate voting <span>');
                            return true;
                        }
                        common_1.debugLog('voting - clickEffect - click handler', 'el =', el, '; evtTargetEl = ', evtTargetEl, '; evt =', evt);
                        var animEl = $('<i>')
                            .addClass(el.prop('class').includes('up') ? 'fa-arrow-alt-up' : 'fa-arrow-alt-down')
                            .addClass('fas position-absolute')
                            .addClass(styles_1.voteAnimCls);
                        el.prepend(animEl);
                        el.css({ position: 'relative' });
                        setTimeout(function () { return animEl.remove(); }, 1000);
                        return true;
                    });
                }
                return [2 /*return*/, Promise.resolve()];
            });
        });
    };
    ;
    VotingModule.prototype.onContentChange = function (args, cfg) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.setup(args, cfg)];
            });
        });
    };
    return VotingModule;
}(RuqESModule_1.RuqESModule));
exports.VotingModule = VotingModule;


/***/ }),
/* 14 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SidebarModule = void 0;
var selectors_1 = __webpack_require__(3);
var styles_1 = __webpack_require__(1);
var common_1 = __webpack_require__(0);
var RuqESModule_1 = __webpack_require__(2);
var SidebarModule = /** @class */ (function (_super) {
    __extends(SidebarModule, _super);
    function SidebarModule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SidebarModule.prototype.toggleSidebar = function () {
        var ico = common_1.$c(styles_1.sidebarButtonIconCls);
        ico.toggleClass('opened');
        var opened = ico.hasClass('opened');
        common_1.setClass(selectors_1.getRightSidebarEl(), styles_1.sidebarClosedCls, !opened);
    };
    ;
    SidebarModule.prototype.setup = function (args, cfg) {
        return __awaiter(this, void 0, void 0, function () {
            var sidebarCfg, iconEl, btnEl;
            return __generator(this, function (_a) {
                sidebarCfg = cfg.sidebar || {};
                common_1.debugLog('setupSidebar', sidebarCfg);
                if (sidebarCfg.rightButton) {
                    iconEl = $('<span>').addClass(styles_1.sidebarButtonIconCls).addClass('opened');
                    btnEl = $('<a>').addClass('btn').css({ paddingTop: 0 }).html(iconEl).click(this.toggleSidebar);
                    selectors_1.getRightSidebarEl().prepend(btnEl);
                    if (sidebarCfg.autoHideRight) {
                        this.toggleSidebar();
                    }
                }
                return [2 /*return*/];
            });
        });
    };
    return SidebarModule;
}(RuqESModule_1.RuqESModule));
exports.SidebarModule = SidebarModule;


/***/ }),
/* 15 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommentModule = void 0;
var common_1 = __webpack_require__(0);
var styles_1 = __webpack_require__(1);
var RuqESModule_1 = __webpack_require__(2);
var genCommentId = function (x) { return "" + styles_1.clsPrefix + x; };
var processedCommentBoxMarkCls = genCommentId('processed-comment-box');
var CommentModule = /** @class */ (function (_super) {
    __extends(CommentModule, _super);
    function CommentModule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    CommentModule.prototype.setupCtrlEnterToSend = function () {
        $('.comment-box')
            .filter(function (_, el) { return !$(el).hasClass(processedCommentBoxMarkCls); })
            .keydown(function (evt) {
            if (evt.key === 'Enter' && (evt.ctrlKey || evt.metaKey)) {
                common_1.debugLog('setupCtrlEnterToSend', 'CTRL+Enter detected', evt);
                var btn = $(evt.target).parent().find('a:contains(Comment)');
                if (btn.length > 1) {
                    common_1.printError('setupCtrlEnterToSend', 'found multiple send buttons', btn);
                }
                else if (btn.length === 0) {
                    common_1.printError('setupCtrlEnterToSend', 'no send button found', btn);
                }
                else {
                    common_1.debugLog('setupCtrlEnterToSend', 'clicking on send button', btn);
                    btn.click();
                }
            }
        })
            .addClass(processedCommentBoxMarkCls);
    };
    ;
    CommentModule.prototype.setup = function (args, cfg) {
        return __awaiter(this, void 0, void 0, function () {
            var commentCfg;
            return __generator(this, function (_a) {
                commentCfg = cfg.comment || {};
                if (!R.prop('silent', args)) {
                    common_1.debugLog('setupComment', commentCfg);
                }
                if (commentCfg.ctrlEnterToSend) {
                    setInterval(this.setupCtrlEnterToSend, 1000);
                }
                return [2 /*return*/];
            });
        });
    };
    return CommentModule;
}(RuqESModule_1.RuqESModule));
exports.CommentModule = CommentModule;


/***/ }),
/* 16 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CreatePostModule = void 0;
var common_1 = __webpack_require__(0);
var logo_svg_1 = __importDefault(__webpack_require__(4));
var styles_1 = __webpack_require__(1);
var selectors_1 = __webpack_require__(3);
var RuqESModule_1 = __webpack_require__(2);
var CreatePostModule = /** @class */ (function (_super) {
    __extends(CreatePostModule, _super);
    function CreatePostModule() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.onLoadTitleButtonClick = function (el) { return function (evt) {
            common_1.debugLog('onLoadTitleButtonClick', el, evt);
            if (el.hasClass(styles_1.loadTitleButtonLoadingCls)) {
                common_1.debugLog('onLoadTitleButtonClick - ignoring click, loading already in progress');
                return;
            }
            var url = String(selectors_1.getPostUrlInPostCreation().val());
            if (!url || (!url.startsWith('https://') && !url.startsWith('http://'))) {
                common_1.debugLog('onLoadTitleButtonClick - ignoring click, url doesn\'t look valid', url);
                return;
            }
            el.prop('disabled', true).addClass(styles_1.loadTitleButtonLoadingCls);
            var enable = function () { return el.prop('disabled', false).removeClass(styles_1.loadTitleButtonLoadingCls); };
            common_1.xhr({
                method: 'GET',
                url: url,
                onload: function (resp) {
                    if (resp.status !== 200) {
                        return;
                    }
                    var parsed = $('<div>').append($(resp.response));
                    var titleFromItemPropName = parsed.find('meta[itemprop=name]').prop('content');
                    var titleFromTitleTag = parsed.find('title').first().text();
                    common_1.debugLog('createPost - possible titles', { titleFromItemPropName: titleFromItemPropName, titleFromTitleTag: titleFromTitleTag });
                    var title = titleFromItemPropName || titleFromTitleTag;
                    if (!title) {
                        common_1.debugLog('failed to get title', parsed);
                    }
                    selectors_1.getPostTitleInPostCreation().val(title);
                    enable();
                },
                onerror: function (err) {
                    common_1.debugLog('post loading failed', err);
                    enable();
                },
            });
        }; };
        _this.appendElToUrlLabel = function (el) {
            return selectors_1.getPostUrlInPostCreation()
                .parent()
                .find('label')
                .append(el)
                .addClass('d-flex justify-content-between align-items-end');
        };
        _this.setupLoadTitleButton = function (cfg) {
            if (!(cfg === null || cfg === void 0 ? void 0 : cfg.loadTitleButton)) {
                return;
            }
            if (window.location.pathname !== '/submit') {
                return;
            }
            var onlyForYouTube = false;
            var btn = $('<a>')
                .addClass('btn btn-secondary')
                .html(logo_svg_1.default)
                .append('Load title')
                .addClass(styles_1.loadTitleButtonCls)
                .hide();
            if (!onlyForYouTube) {
                btn.show();
            }
            selectors_1.getPostUrlInPostCreation().change(function (evt) {
                var val = evt.target.value;
                var isYT = val.includes('youtube.com') || val.includes('youtu.be');
                if ((onlyForYouTube && isYT) || (!onlyForYouTube)) {
                    btn.show();
                }
                else {
                    btn.hide();
                }
            });
            _this.appendElToUrlLabel(btn);
            btn.click(_this.onLoadTitleButtonClick(btn));
        };
        _this.addLoadingEffectsToButton = function (btn) {
            btn.prop('disabled', true).addClass(styles_1.uploadImageToImgbbButtonLoadingCls);
        };
        _this.removeLoadingEffectsFromButton = function (btn) {
            btn.prop('disabled', false).removeClass(styles_1.uploadImageToImgbbButtonLoadingCls);
        };
        _this.fillUrl = function (url) {
            selectors_1.getPostUrlInPostCreation().val(url).trigger('input');
        };
        _this.onUploadImageToImgbbClick = function (cfg) { return function (el) { return function (evt) {
            common_1.debugLog('onUploadImageToImgbbClick', cfg, el, evt);
            if (el.hasClass(styles_1.uploadImageToImgbbButtonLoadingCls)) {
                common_1.debugLog('onLoadTitleButtonClick - ignoring click, loading already in progress');
                return;
            }
            var enable = function () { return _this.removeLoadingEffectsFromButton(el); };
            var onError = function (err) {
                common_1.debugLog('onError', err);
                alert("Upload failed: " + err.status + "\n" + err.response);
                enable();
            };
            var fileInput = $('<input type="file" class="d-none" accept="image/x-png,image/gif,image/jpeg">');
            fileInput.change(function (evt) {
                common_1.debugLog('fileInput.change', evt);
                var files = fileInput.prop('files');
                if (!files || !files[0]) {
                    return;
                }
                _this.addLoadingEffectsToButton(el);
                var file = files[0];
                common_1.debugLog('got pic for upload', file);
                if (file) {
                    _this.uploadImageToImgBb({
                        cfg: cfg,
                        file: file,
                        onSuccess: function (url) {
                            _this.fillUrl(url);
                            enable();
                        },
                        onError: onError,
                    });
                }
            });
            $('body').append(fileInput);
            fileInput.click();
        }; }; };
        _this.uploadImageToImgBb = function (args) {
            var cfg = args.cfg, file = args.file, onError = args.onError, onSuccess = args.onSuccess;
            common_1.debugLog('uploadImageToImgBb', { cfg: cfg, file: file });
            var doRequest = function (image) {
                var _a;
                common_1.debugLog('doRequest');
                var fd = new FormData();
                fd.append('image', btoa(image));
                var data = fd;
                common_1.debugLog({ data: data });
                common_1.xhr({
                    method: 'POST',
                    url: "https://api.imgbb.com/1/upload?key=" + ((_a = cfg === null || cfg === void 0 ? void 0 : cfg.external) === null || _a === void 0 ? void 0 : _a.imgbbKey),
                    data: data,
                    onload: function (resp) {
                        if (resp.status !== 200) {
                            return onError(resp);
                        }
                        common_1.debugLog('imgbb upload success', resp, resp.response);
                        var json = null;
                        try {
                            json = JSON.parse(resp.response);
                        }
                        catch (e) { }
                        if (json) {
                            var url = String(R.path(['data', 'url'])(json));
                            common_1.debugLog('parsed imgbb response, url is', url, '; whole json = ', json);
                            onSuccess(url);
                        }
                        else {
                            alert("Failed to parse response from ImgBB:\n" + resp.response);
                            onError(resp);
                        }
                    },
                    onerror: function (err) { return onError(err); },
                });
            };
            var reader = new FileReader();
            reader.onload = function (readEvt) {
                var _a;
                doRequest((_a = readEvt.target) === null || _a === void 0 ? void 0 : _a.result);
            };
            reader.readAsBinaryString(file);
        };
        _this.setupImgbbUploadButton = function (cfg) {
            var _a;
            if (!((_a = cfg === null || cfg === void 0 ? void 0 : cfg.external) === null || _a === void 0 ? void 0 : _a.imgbbKey)) {
                return;
            }
            if (window.location.pathname !== '/submit') {
                return;
            }
            var btn = $('<a>')
                .addClass('btn btn-secondary')
                .html(logo_svg_1.default)
                .append('Upload image')
                .addClass(styles_1.uploadImageToImgbbButtonCls);
            var removeHighlight = function () { return btn.removeClass(styles_1.uploadImageToImgbbButtonHighlightedCls); };
            var onDropOnImgBbUploadButton = function (evt) {
                common_1.preventDefaults(evt);
                if (btn.hasClass(styles_1.uploadImageToImgbbButtonLoadingCls)) {
                    common_1.debugLog('setupImgbbUploadButton - ignoring, loading already in progress', evt);
                    return;
                }
                var dt = evt.dataTransfer || evt.originalEvent.dataTransfer;
                var files = __spread(dt.files);
                common_1.debugLog('setupImgbbUploadButton', 'drop', dt || 'missing DT', files || 'missing files', evt);
                removeHighlight();
                _this.addLoadingEffectsToButton(btn);
                var file = files[0];
                if (file) {
                    _this.uploadImageToImgBb({
                        cfg: cfg,
                        file: file,
                        onError: function (resp) {
                            _this.removeLoadingEffectsFromButton(btn);
                            alert("drop upload failed: " + resp.status + "\n" + resp.response);
                        },
                        onSuccess: function (url) {
                            _this.removeLoadingEffectsFromButton(btn);
                            _this.fillUrl(url);
                        },
                    });
                }
            };
            btn
                .on('dragover', function (evt) {
                common_1.preventDefaults(evt);
                btn.addClass(styles_1.uploadImageToImgbbButtonHighlightedCls);
            })
                .on('dragleave', function (evt) {
                common_1.preventDefaults(evt);
                removeHighlight();
            })
                .on('drop', onDropOnImgBbUploadButton);
            _this.appendElToUrlLabel(btn);
            btn.click(_this.onUploadImageToImgbbClick(cfg)(btn));
        };
        return _this;
    }
    CreatePostModule.prototype.setup = function (args, cfg) {
        return __awaiter(this, void 0, void 0, function () {
            var creatPostCfg;
            return __generator(this, function (_a) {
                creatPostCfg = cfg.createPost || {};
                if (!R.prop('silent', args || {})) {
                    common_1.debugLog('setupCreatePost');
                }
                this.appendElToUrlLabel($('<i>').css({ flexGrow: 10 }));
                this.setupLoadTitleButton(creatPostCfg);
                this.setupImgbbUploadButton(cfg);
                return [2 /*return*/];
            });
        });
    };
    ;
    return CreatePostModule;
}(RuqESModule_1.RuqESModule));
exports.CreatePostModule = CreatePostModule;


/***/ }),
/* 17 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.InfiniteScrollModule = void 0;
var selectors_1 = __webpack_require__(3);
var common_1 = __webpack_require__(0);
var styles_1 = __webpack_require__(1);
var modules_1 = __webpack_require__(5);
var RuqESModule_1 = __webpack_require__(2);
var coolDownWhenCombinedWithFullHidingOfPosts = 3000;
var InfiniteScrollModule = /** @class */ (function (_super) {
    __extends(InfiniteScrollModule, _super);
    function InfiniteScrollModule() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.infiniteScrollState = {
            loading: false,
            nextPageUrl: null,
            coolDownActive: false,
            coolDownTimeoutId: null,
            loadNextPageAfterCoolDownFinishes: false,
        };
        _this.onNextPageSuccessResponse = function (resp) { return __awaiter(_this, void 0, void 0, function () {
            var parsed, newData, newPage, pageNum, pageCounter;
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (resp.status !== 200) {
                            return [2 /*return*/, this.onNextPageErrorResponse(resp)];
                        }
                        parsed = $(resp.response);
                        common_1.debugLog('onNextPageSuccessResponse', resp);
                        newData = parsed.find('#posts');
                        if (!newData.length) {
                            newData = parsed.find('.guild-border-top').next();
                        }
                        if (!newData.length) {
                            newData = parsed.find('.posts');
                        }
                        if (!newData.length) {
                            common_1.printError('InfiniteScroll failed to locate data in response.', parsed);
                            selectors_1.getPageLoadingMarker().html('[RuqES] Failed to locate data in response.');
                            selectors_1.getPagination().css('display', 'block');
                            return [2 /*return*/];
                        }
                        newPage = $('<div>')
                            .prop('id', "page-" + 0)
                            .html(newData)
                            .append(selectors_1.getPagination(parsed));
                        pageNum = R.prop(1, __spread((_a = this.infiniteScrollState.nextPageUrl) === null || _a === void 0 ? void 0 : _a.match(/page=(\d+)/)));
                        pageCounter = $('<div>').text("Page " + (pageNum || '?'))
                            .addClass(['card', 'text-center', 'py-1']);
                        selectors_1.getPagination().after(newPage).after(pageCounter).remove();
                        selectors_1.getPageLoadingMarker().remove();
                        this.infiniteScrollState.loading = false;
                        this.infiniteScrollState.nextPageUrl = null;
                        return [4 /*yield*/, modules_1.handleModulesAfterContentChange()];
                    case 1:
                        _b.sent();
                        return [2 /*return*/];
                }
            });
        }); };
        _this.onNextPageErrorResponse = function (resp) {
            console.error(common_1.logPrefix, 'loading of next page FAILED', resp);
            var textEl = $('<pre>').addClass(styles_1.codeBlockCls).html($('<code>').text(resp.responseText));
            var tryManuallyBtn = $('<a>')
                .text('Go to next page manually')
                .prop('href', _this.infiniteScrollState.nextPageUrl);
            var retryBtn = $('<button>')
                .text('Retry')
                .addClass('btn')
                .addClass('btn-primary')
                .addClass('ml-1')
                .click(function () {
                _this.infiniteScrollState.loading = false;
                selectors_1.getPageLoadingMarker().remove();
                _this.startLoadingNextPage();
            });
            var errorEl = $('<div>')
                .addClass(styles_1.pageLoadingErrorCls)
                .append($('<div>').text("Error " + resp.status + ": " + resp.statusText).append(retryBtn))
                .append(tryManuallyBtn)
                .append(textEl);
            selectors_1.getPageLoadingMarker().html(errorEl);
        };
        _this.startLoadingNextPage = function () {
            if (_this.infiniteScrollState.coolDownActive) {
                _this.infiniteScrollState.loadNextPageAfterCoolDownFinishes = true;
                if (common_1.$c(styles_1.infiniteScrollCoolDownMarkerCls).length === 0) {
                    var cdMarker = $('<span>⏳</span>').addClass(styles_1.infiniteScrollCoolDownMarkerCls);
                    selectors_1.getPagination().find('ul').append(cdMarker);
                }
                return;
            }
            var nextUrl = selectors_1.getNextPageLink().prop('href');
            common_1.debugLog('InfiniteScroll starts fetching', nextUrl);
            if (!nextUrl) {
                return;
            }
            _this.infiniteScrollState.loading = true;
            _this.infiniteScrollState.nextPageUrl = nextUrl;
            common_1.xhr({
                method: 'GET',
                url: nextUrl,
                onload: _this.onNextPageSuccessResponse,
                onerror: _this.onNextPageErrorResponse,
            });
            var loader = common_1.createTextLoader('Loading next page', styles_1.pageLoadingCls).addClass('h4');
            selectors_1.getPagination()
                .css('display', 'none')
                .after(loader);
        };
        _this.onNextPageLinkScrolledTo = function () {
            if (_this.infiniteScrollState.loading) {
                return;
            }
            _this.startLoadingNextPage();
        };
        _this.isValidPathForInfiniteScroll = function (path) { return true; };
        _this.activateInfiniteScrollCoolDown = function () {
            common_1.debugLog('activateInfiniteScrollCoolDown called');
            if (_this.infiniteScrollState.coolDownActive && _this.infiniteScrollState.coolDownTimeoutId) {
                clearTimeout(_this.infiniteScrollState.coolDownTimeoutId);
            }
            _this.infiniteScrollState.coolDownActive = true;
            _this.infiniteScrollState.coolDownTimeoutId = setTimeout(function () {
                common_1.debugLog('activateInfiniteScrollCoolDown - cool down ended');
                _this.infiniteScrollState.coolDownActive = false;
                _this.infiniteScrollState.coolDownTimeoutId = null;
                if (_this.infiniteScrollState.loadNextPageAfterCoolDownFinishes) {
                    _this.startLoadingNextPage();
                    _this.infiniteScrollState.loadNextPageAfterCoolDownFinishes = false;
                }
            }, coolDownWhenCombinedWithFullHidingOfPosts);
        };
        _this.checkScrollPosition = function (cfg) { return function () {
            var doc = $(document);
            if (!_this.isValidPathForInfiniteScroll(window.location.pathname)) {
                return;
            }
            var nextPageLink = selectors_1.getNextPageLink();
            if (!nextPageLink[0]) {
                return;
            }
            var offset = cfg.infiniteScroll.loadEarlier ? -2000 : 0;
            if ((doc.scrollTop() || 0) + ($(window).height() || 0) >= nextPageLink.position().top + offset) {
                _this.onNextPageLinkScrolledTo();
                if (cfg.infiniteScroll.loadEarlier && !_this.infiniteScrollState.coolDownActive) {
                    _this.activateInfiniteScrollCoolDown();
                }
            }
        }; };
        return _this;
    }
    InfiniteScrollModule.prototype.setup = function (args, cfg) {
        var _a;
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_b) {
                if (!((_a = cfg === null || cfg === void 0 ? void 0 : cfg.infiniteScroll) === null || _a === void 0 ? void 0 : _a.enabled)) {
                    return [2 /*return*/];
                }
                common_1.debugLog('setupInfiniteScroll');
                $(document).on('scroll', this.checkScrollPosition(cfg));
                $('#main-content-col').css('margin-bottom', '2em');
                setTimeout(this.checkScrollPosition(cfg), 100);
                return [2 /*return*/];
            });
        });
    };
    ;
    InfiniteScrollModule.prototype.onSomePostsFullyHidden = function (cfg) {
        this.activateInfiniteScrollCoolDown();
    };
    return InfiniteScrollModule;
}(RuqESModule_1.RuqESModule));
exports.InfiniteScrollModule = InfiniteScrollModule;


/***/ }),
/* 18 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PostsModule = void 0;
var styles_1 = __webpack_require__(1);
var common_1 = __webpack_require__(0);
var RuqESModule_1 = __webpack_require__(2);
var PostsModule = /** @class */ (function (_super) {
    __extends(PostsModule, _super);
    function PostsModule() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.isGuildHidden = function (cfg, guildName) {
            if (guildName === void 0) { guildName = ''; }
            var hiddenGuilds = ((cfg === null || cfg === void 0 ? void 0 : cfg.hidePostsFromGuilds) || '').split(',').map(function (x) { return x.toLowerCase(); });
            var processedGuildName = common_1.dropWhileString(function (x) { return x === '+'; }, guildName.toLowerCase());
            return hiddenGuilds.includes(processedGuildName);
        };
        _this.isPostFromHiddenGuild = function (cfg) { return function (_, el) {
            var guildName = $(el).find('.post-meta-guild a').filter(function (_, el) { return $(el).text().startsWith('+'); }).first().text();
            return _this.isGuildHidden(cfg, guildName);
        }; };
        _this.isOnDiscoverPage = function () { return window.location.pathname === '/browse'; };
        return _this;
    }
    PostsModule.prototype.hideAlreadyJoinedGuildTiles = function () {
        $('#main-content-col .card-footer :not(.d-none) > .btn-secondary')
            .each(function (_, rawEl) { $(rawEl).closest('.col').remove(); });
    };
    PostsModule.prototype.setup = function (args, cfg) {
        var _a, _b;
        return __awaiter(this, void 0, void 0, function () {
            var postsCfg, toHide;
            return __generator(this, function (_c) {
                postsCfg = cfg.post || {};
                if (!R.prop('silent', args || {})) {
                    common_1.debugLog('setupPosts', postsCfg);
                }
                if (postsCfg.openInNewTab) {
                    $('.post-title a').prop('target', '_blank');
                    $('.post-actions a > i.fa-comment-dots').parent().prop('target', '_blank');
                }
                if (postsCfg.hidePostsFromGuilds) {
                    toHide = $('#posts > .card').filter(this.isPostFromHiddenGuild(postsCfg));
                    if (postsCfg.fullyHidePostsFromGuilds) {
                        toHide.hide();
                        (_a = this.somePostsFullyHiddenCb) === null || _a === void 0 ? void 0 : _a.call(this);
                    }
                    else {
                        toHide.addClass(styles_1.semiHiddenPostFromGuildCls);
                    }
                }
                if (postsCfg.blurNsfwThumbnails) {
                    $('body').addClass(styles_1.blurNsfwThumbnailsCls);
                }
                if (postsCfg.hideAlreadyJoinedGuildsInDiscovery && this.isOnDiscoverPage()) {
                    this.hideAlreadyJoinedGuildTiles();
                    (_b = this.somePostsFullyHiddenCb) === null || _b === void 0 ? void 0 : _b.call(this);
                }
                if (postsCfg.improvedTableStyles) {
                    $("." + styles_1.boxPostTextCls + ", #post-body").addClass(styles_1.improvedTableCls);
                }
                if (postsCfg.lessAggressivePatronBadges) {
                    $('.post-meta .badge, .user-info .badge').filter(function (_, el) { return $(el).text().includes('Patron'); }).addClass(styles_1.nonAggressiveCls);
                }
                return [2 /*return*/];
            });
        });
    };
    ;
    PostsModule.prototype.onContentChange = function (args, cfg) {
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.setup(args, cfg)];
            });
        });
    };
    return PostsModule;
}(RuqESModule_1.RuqESModule));
exports.PostsModule = PostsModule;


/***/ })
/******/ ]);