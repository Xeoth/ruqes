const headersGenerator = require('./headersGenerator');
const path = require('path');
const WebpackUserscript = require('webpack-userscript');
const dev = process.env.NODE_ENV === 'development';

module.exports = {
    mode: dev ? 'development' : 'production',
    entry: path.resolve(__dirname, 'src', 'app.ts'),
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'ruqes.user.js',
    },
    optimization: {
        minimize: false,
    },
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
    },
    resolve: {
        modules: [
            path.resolve(__dirname, './src'),
            'node_modules',
        ],
        extensions: ['.js', '.ts'],
    },
    plugins: [
        new WebpackUserscript({
            headers: headersGenerator,
        }),
    ],
    module: {
        rules: [
            {
                test: /\.(svg|css)$/i,
                use: 'raw-loader',
            },
            {
                test: /\.tsx?$/,
                loader: 'ts-loader',
            },
        ],
    },
    externals: {
    },
}
